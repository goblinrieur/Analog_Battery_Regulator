**Welcome on a FORK of abandoned "the Open Small Wind Turbine Analog Charge Controller (OSWACC) project."**

The fork objective are currently about 

- [X] Keep original license 

- [X] Prepare directories 

- [X] As a fork change tags branches and version to v3.3.0 

- [X] remove useless branches and files

- [ ] Improving PCB to a smaller double-sided PCB design (_work in progress_)

- [X] Update existing schematic, footprints, etc , to kicad 8.x.x compatibility with no structural changes  

- [X] Adapt to my own need in output voltage and max power 

- [X] Adapt to my own need for a double sided PCB and designs

- [ ] Adapt the 3D printable box

**Pictures**

![1](../03-Documents/1.png)

![2](../03-Documents/2.png)

![3](../03-Documents/3.png)

![4](../03-Documents/4.png)

![5](../03-Documents/5.png)

![6](../03-Documents/6.png)

![7](../03-Documents/7.png)

**All above is from original project before the fork**

See Original Readme [here](./OriginalReadMe.md)

Original project [page](https://gitlab.com/TiEoLibre/Analog_Battery_Regulator/-/blob/master/README.md)
