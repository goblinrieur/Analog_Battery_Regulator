(kicad_sch
	(version 20231120)
	(generator "eeschema")
	(generator_version "8.0")
	(uuid "b4cce7ed-771b-4bd3-a414-5049c1e45b26")
	(paper "A3")
	(title_block
		(title "SWACC - Small Wind Charge Controller - fork ")
		(date "2024-06-01")
		(rev "v3.3.0")
		(company "Tripalium ")
		(comment 1 "Original Design & Schematic : Adrien Prévost - adriprevost@hotmail.fr")
		(comment 2 "Contributors : Adrien Prévost, Clément Gangneux, Bastien Gary, Jay Hudnall")
		(comment 3 "Contributor to the fork : Francois Pussault -> v3.3.0")
	)
	(lib_symbols
		(symbol "Device:C"
			(pin_numbers hide)
			(pin_names
				(offset 0.254)
			)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "C"
				(at 0.635 2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Value" "C"
				(at 0.635 -2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Footprint" ""
				(at 0.9652 -3.81 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" "~"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" "Unpolarized capacitor"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_keywords" "cap capacitor"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "C_*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "C_0_1"
				(polyline
					(pts
						(xy -2.032 -0.762) (xy 2.032 -0.762)
					)
					(stroke
						(width 0.508)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy -2.032 0.762) (xy 2.032 0.762)
					)
					(stroke
						(width 0.508)
						(type default)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "C_1_1"
				(pin passive line
					(at 0 3.81 270)
					(length 2.794)
					(name "~"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 0 -3.81 90)
					(length 2.794)
					(name "~"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "Device:D"
			(pin_numbers hide)
			(pin_names
				(offset 1.016) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "D"
				(at 0 2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Value" "D"
				(at 0 -2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" "~"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" "Diode"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Sim.Device" "D"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Sim.Pins" "1=K 2=A"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_keywords" "diode"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "TO-???* *_Diode_* *SingleDiode* D_*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "D_0_1"
				(polyline
					(pts
						(xy -1.27 1.27) (xy -1.27 -1.27)
					)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 1.27 0) (xy -1.27 0)
					)
					(stroke
						(width 0)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 1.27 1.27) (xy 1.27 -1.27) (xy -1.27 0) (xy 1.27 1.27)
					)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "D_1_1"
				(pin passive line
					(at -3.81 0 0)
					(length 2.54)
					(name "K"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 3.81 0 180)
					(length 2.54)
					(name "A"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "Device:D_Zener"
			(pin_numbers hide)
			(pin_names
				(offset 1.016) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "D"
				(at 0 2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Value" "D_Zener"
				(at 0 -2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" "~"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" "Zener diode"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_keywords" "diode"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "TO-???* *_Diode_* *SingleDiode* D_*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "D_Zener_0_1"
				(polyline
					(pts
						(xy 1.27 0) (xy -1.27 0)
					)
					(stroke
						(width 0)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy -1.27 -1.27) (xy -1.27 1.27) (xy -0.762 1.27)
					)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 1.27 -1.27) (xy 1.27 1.27) (xy -1.27 0) (xy 1.27 -1.27)
					)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "D_Zener_1_1"
				(pin passive line
					(at -3.81 0 0)
					(length 2.54)
					(name "K"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 3.81 0 180)
					(length 2.54)
					(name "A"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "Device:LED"
			(pin_numbers hide)
			(pin_names
				(offset 1.016) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "D"
				(at 0 2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Value" "LED"
				(at 0 -2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" "~"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" "Light emitting diode"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_keywords" "LED diode"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "LED* LED_SMD:* LED_THT:*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "LED_0_1"
				(polyline
					(pts
						(xy -1.27 -1.27) (xy -1.27 1.27)
					)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy -1.27 0) (xy 1.27 0)
					)
					(stroke
						(width 0)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 1.27 -1.27) (xy 1.27 1.27) (xy -1.27 0) (xy 1.27 -1.27)
					)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy -3.048 -0.762) (xy -4.572 -2.286) (xy -3.81 -2.286) (xy -4.572 -2.286) (xy -4.572 -1.524)
					)
					(stroke
						(width 0)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy -1.778 -0.762) (xy -3.302 -2.286) (xy -2.54 -2.286) (xy -3.302 -2.286) (xy -3.302 -1.524)
					)
					(stroke
						(width 0)
						(type default)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "LED_1_1"
				(pin passive line
					(at -3.81 0 0)
					(length 2.54)
					(name "K"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 3.81 0 180)
					(length 2.54)
					(name "A"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "Device:Q_NPN_BCE"
			(pin_names
				(offset 0) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "Q"
				(at 5.08 1.27 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Value" "Q_NPN_BCE"
				(at 5.08 -1.27 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Footprint" ""
				(at 5.08 2.54 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" "~"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" "NPN transistor, base/collector/emitter"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_keywords" "transistor NPN"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "Q_NPN_BCE_0_1"
				(polyline
					(pts
						(xy 0.635 0.635) (xy 2.54 2.54)
					)
					(stroke
						(width 0)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 0.635 -0.635) (xy 2.54 -2.54) (xy 2.54 -2.54)
					)
					(stroke
						(width 0)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 0.635 1.905) (xy 0.635 -1.905) (xy 0.635 -1.905)
					)
					(stroke
						(width 0.508)
						(type default)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 1.27 -1.778) (xy 1.778 -1.27) (xy 2.286 -2.286) (xy 1.27 -1.778) (xy 1.27 -1.778)
					)
					(stroke
						(width 0)
						(type default)
					)
					(fill
						(type outline)
					)
				)
				(circle
					(center 1.27 0)
					(radius 2.8194)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "Q_NPN_BCE_1_1"
				(pin input line
					(at -5.08 0 0)
					(length 5.715)
					(name "B"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 2.54 5.08 270)
					(length 2.54)
					(name "C"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 2.54 -5.08 90)
					(length 2.54)
					(name "E"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "Device:R"
			(pin_numbers hide)
			(pin_names
				(offset 0)
			)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "R"
				(at 2.032 0 90)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Value" "R"
				(at 0 0 90)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at -1.778 0 90)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" "~"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" "Resistor"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_keywords" "R res resistor"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "R_*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "R_0_1"
				(rectangle
					(start -1.016 -2.54)
					(end 1.016 2.54)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "R_1_1"
				(pin passive line
					(at 0 3.81 270)
					(length 1.27)
					(name "~"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 0 -3.81 90)
					(length 1.27)
					(name "~"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "Mechanical:MountingHole_Pad"
			(pin_numbers hide)
			(pin_names
				(offset 1.016) hide)
			(exclude_from_sim yes)
			(in_bom no)
			(on_board yes)
			(property "Reference" "H"
				(at 0 6.35 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Value" "MountingHole_Pad"
				(at 0 4.445 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" "~"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" "Mounting Hole with connection"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_keywords" "mounting hole"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "MountingHole*Pad*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "MountingHole_Pad_0_1"
				(circle
					(center 0 1.27)
					(radius 1.27)
					(stroke
						(width 1.27)
						(type default)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "MountingHole_Pad_1_1"
				(pin input line
					(at 0 -2.54 90)
					(length 2.54)
					(name "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "RegulatorV2-rescue:Conn_01x03"
			(pin_names
				(offset 1.016) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "J"
				(at 0 5.08 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Value" "Conn_01x03"
				(at 0 -5.08 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "Connector*:*_1x??_*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "Conn_01x03_1_1"
				(rectangle
					(start -1.27 -2.413)
					(end 0 -2.667)
					(stroke
						(width 0.1524)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(rectangle
					(start -1.27 0.127)
					(end 0 -0.127)
					(stroke
						(width 0.1524)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(rectangle
					(start -1.27 2.667)
					(end 0 2.413)
					(stroke
						(width 0.1524)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(rectangle
					(start -1.27 3.81)
					(end 1.27 -3.81)
					(stroke
						(width 0.254)
						(type solid)
					)
					(fill
						(type background)
					)
				)
				(pin passive line
					(at -5.08 2.54 0)
					(length 3.81)
					(name "Pin_1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -5.08 0 0)
					(length 3.81)
					(name "Pin_2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -5.08 -2.54 0)
					(length 3.81)
					(name "Pin_3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "RegulatorV2-rescue:GND-power"
			(power)
			(pin_names
				(offset 0)
			)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "#PWR"
				(at 0 -6.35 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Value" "power:GND"
				(at 0 -3.81 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "GND-power_0_1"
				(polyline
					(pts
						(xy 0 0) (xy 0 -1.27) (xy 1.27 -1.27) (xy 0 -2.54) (xy -1.27 -1.27) (xy 0 -1.27)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "GND-power_1_1"
				(pin power_in line
					(at 0 0 270)
					(length 0) hide
					(name "GND"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "RegulatorV2-rescue:IRLZ44N"
			(pin_names
				(offset 0) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "Q"
				(at 6.35 1.905 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Value" "IRLZ44N"
				(at 6.35 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Footprint" "Package_TO_SOT_THT:TO-220-3_Vertical"
				(at 6.35 -1.905 0)
				(effects
					(font
						(size 1.27 1.27)
						(italic yes)
					)
					(justify left)
					(hide yes)
				)
			)
			(property "Datasheet" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
					(hide yes)
				)
			)
			(property "Description" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "TO?220*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "IRLZ44N_0_1"
				(polyline
					(pts
						(xy 0.762 -1.778) (xy 2.54 -1.778)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 0.762 -1.27) (xy 0.762 -2.286)
					)
					(stroke
						(width 0.254)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 0.762 0) (xy 2.54 0)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 0.762 0.508) (xy 0.762 -0.508)
					)
					(stroke
						(width 0.254)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 0.762 1.778) (xy 2.54 1.778)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 0.762 2.286) (xy 0.762 1.27)
					)
					(stroke
						(width 0.254)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 2.54 -1.778) (xy 2.54 -2.54)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 2.54 -1.778) (xy 2.54 0)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 2.54 2.54) (xy 2.54 1.778)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 0.254 1.905) (xy 0.254 -1.905) (xy 0.254 -1.905)
					)
					(stroke
						(width 0.254)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 1.016 0) (xy 2.032 0.381) (xy 2.032 -0.381) (xy 1.016 0)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type outline)
					)
				)
				(polyline
					(pts
						(xy 2.54 -1.778) (xy 3.302 -1.778) (xy 3.302 1.778) (xy 2.54 1.778)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 2.794 0.508) (xy 2.921 0.381) (xy 3.683 0.381) (xy 3.81 0.254)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 3.302 0.381) (xy 2.921 -0.254) (xy 3.683 -0.254) (xy 3.302 0.381)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(circle
					(center 1.651 0)
					(radius 2.8194)
					(stroke
						(width 0.254)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(circle
					(center 2.54 -1.778)
					(radius 0.2794)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type outline)
					)
				)
				(circle
					(center 2.54 1.778)
					(radius 0.2794)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type outline)
					)
				)
			)
			(symbol "IRLZ44N_1_1"
				(pin input line
					(at -5.08 0 0)
					(length 5.334)
					(name "G"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 2.54 5.08 270)
					(length 2.54)
					(name "D"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 2.54 -5.08 90)
					(length 2.54)
					(name "S"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "RegulatorV2-rescue:LM358"
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "U"
				(at 0 5.08 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Value" "LM358"
				(at 0 -5.08 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "SOIC*3.9x4.9mm*P1.27mm* DIP*W7.62mm* TO*99* OnSemi*Micro8* TSSOP*3x3mm*P0.65mm* TSSOP*4.4x3mm*P0.65mm* MSOP*3x3mm*P0.65mm* SSOP*3.9x4.9mm*P0.635mm* LFCSP*2x2mm*P0.5mm* *SIP* SOIC*5.3x6.2mm*P1.27mm*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "LM358_0_1"
				(polyline
					(pts
						(xy -5.08 5.08) (xy 5.08 0) (xy -5.08 -5.08) (xy -5.08 5.08)
					)
					(stroke
						(width 0.254)
						(type solid)
					)
					(fill
						(type background)
					)
				)
				(pin power_in line
					(at -2.54 -7.62 90)
					(length 3.81)
					(name "V-"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "4"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin power_in line
					(at -2.54 7.62 270)
					(length 3.81)
					(name "V+"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "8"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
			(symbol "LM358_1_1"
				(pin output line
					(at 7.62 0 180)
					(length 2.54)
					(name "~"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin input line
					(at -7.62 -2.54 0)
					(length 2.54)
					(name "-"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin input line
					(at -7.62 2.54 0)
					(length 2.54)
					(name "+"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
			(symbol "LM358_2_1"
				(pin input line
					(at -7.62 2.54 0)
					(length 2.54)
					(name "+"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "5"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin input line
					(at -7.62 -2.54 0)
					(length 2.54)
					(name "-"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "6"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin output line
					(at 7.62 0 180)
					(length 2.54)
					(name "~"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "7"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "RegulatorV2-rescue:POT"
			(pin_names
				(offset 1.016) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "RV"
				(at -4.445 0 90)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Value" "POT"
				(at -2.54 0 90)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "Potentiometer*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "POT_0_1"
				(polyline
					(pts
						(xy 2.54 0) (xy 1.524 0)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy 1.143 0) (xy 2.286 0.508) (xy 2.286 -0.508)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type outline)
					)
				)
				(rectangle
					(start 1.016 2.54)
					(end -1.016 -2.54)
					(stroke
						(width 0.254)
						(type solid)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "POT_1_1"
				(pin passive line
					(at 0 3.81 270)
					(length 1.27)
					(name "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 3.81 0 180)
					(length 1.27)
					(name "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 0 -3.81 90)
					(length 1.27)
					(name "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "RegulatorV2-rescue:PWR_FLAG-power"
			(power)
			(pin_numbers hide)
			(pin_names
				(offset 0) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "#FLG"
				(at 0 1.905 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Value" "power:PWR_FLAG"
				(at 0 3.81 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "PWR_FLAG-power_0_0"
				(pin power_out line
					(at 0 0 90)
					(length 0)
					(name "pwr"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
			(symbol "PWR_FLAG-power_0_1"
				(polyline
					(pts
						(xy 0 0) (xy 0 1.27) (xy -1.016 1.905) (xy 0 2.54) (xy 1.016 1.905) (xy 0 1.27)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
			)
		)
		(symbol "RegulatorV2-rescue:SW_SP3T"
			(pin_names
				(offset 0) hide)
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "SW"
				(at 0 5.08 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Value" "SW_SP3T"
				(at 0 -5.08 0)
				(effects
					(font
						(size 1.27 1.27)
					)
				)
			)
			(property "Footprint" ""
				(at -15.875 4.445 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" ""
				(at -15.875 4.445 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "SW* SP3T*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "SW_SP3T_0_1"
				(circle
					(center -2.032 0)
					(radius 0.4572)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(polyline
					(pts
						(xy -1.651 0.254) (xy 1.651 2.286)
					)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(circle
					(center 2.032 -2.54)
					(radius 0.4572)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(circle
					(center 2.032 0)
					(radius 0.4572)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
				(circle
					(center 2.032 2.54)
					(radius 0.4572)
					(stroke
						(width 0)
						(type solid)
					)
					(fill
						(type none)
					)
				)
			)
			(symbol "SW_SP3T_1_1"
				(pin passive line
					(at 5.08 2.54 180)
					(length 2.54)
					(name "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 5.08 0 180)
					(length 2.54)
					(name "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -5.08 0 0)
					(length 2.54)
					(name "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at 5.08 -2.54 180)
					(length 2.54)
					(name "4"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "4"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
		(symbol "Regulator_Controller:SG3525"
			(exclude_from_sim no)
			(in_bom yes)
			(on_board yes)
			(property "Reference" "U"
				(at 3.81 19.05 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Value" "SG3525"
				(at 3.81 16.51 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(justify left)
				)
			)
			(property "Footprint" ""
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Datasheet" "www.st.com/resource/en/datasheet/sg3525.pdf"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "Description" "Regulating Pulse Width Modulators, NOR Logic, PDIP-16/SOIC-16"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_keywords" "SMPS PWM Controller"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(property "ki_fp_filters" "SOIC*16*3.9x9.9mm*P1.27mm* SOIC*16*7.5x10.3mm*P1.27mm* DIP*16*W7.62mm*"
				(at 0 0 0)
				(effects
					(font
						(size 1.27 1.27)
					)
					(hide yes)
				)
			)
			(symbol "SG3525_0_1"
				(rectangle
					(start -10.16 -15.24)
					(end 10.16 15.24)
					(stroke
						(width 0.254)
						(type default)
					)
					(fill
						(type background)
					)
				)
			)
			(symbol "SG3525_1_1"
				(pin input line
					(at -12.7 10.16 0)
					(length 2.54)
					(name "INV"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "1"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -12.7 2.54 0)
					(length 2.54)
					(name "SD"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "10"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin output line
					(at 12.7 2.54 180)
					(length 2.54)
					(name "OUTA"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "11"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin power_in line
					(at 0 -17.78 90)
					(length 2.54)
					(name "GND"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "12"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin power_in line
					(at 2.54 17.78 270)
					(length 2.54)
					(name "VC"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "13"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin output line
					(at 12.7 -2.54 180)
					(length 2.54)
					(name "OUTB"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "14"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin power_in line
					(at 0 17.78 270)
					(length 2.54)
					(name "VIN"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "15"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin power_in line
					(at -12.7 5.08 0)
					(length 2.54)
					(name "REF"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "16"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin input line
					(at -12.7 12.7 0)
					(length 2.54)
					(name "NI"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "2"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -12.7 0 0)
					(length 2.54)
					(name "SYNC"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "3"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin output line
					(at -12.7 -2.54 0)
					(length 2.54)
					(name "OSC"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "4"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -12.7 -7.62 0)
					(length 2.54)
					(name "CT"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "5"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -12.7 -5.08 0)
					(length 2.54)
					(name "RT"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "6"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin open_collector line
					(at -12.7 -10.16 0)
					(length 2.54)
					(name "DISCH"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "7"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -12.7 -12.7 0)
					(length 2.54)
					(name "SS"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "8"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
				(pin passive line
					(at -12.7 7.62 0)
					(length 2.54)
					(name "COMP"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
					(number "9"
						(effects
							(font
								(size 1.27 1.27)
							)
						)
					)
				)
			)
		)
	)
	(junction
		(at 35.56 168.91)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "01f3bb69-ceb1-4ace-aef6-057e98021494")
	)
	(junction
		(at 255.27 69.85)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "1b38be81-0f9b-4050-a92b-ebb6adfcd63b")
	)
	(junction
		(at 204.47 182.88)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "2aceb822-b264-4891-8412-95df5968d0dc")
	)
	(junction
		(at 67.31 124.46)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "2bf59383-d051-4e8f-9a13-be71d3a9eab0")
	)
	(junction
		(at 59.69 171.45)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "3071c3a6-5ede-4353-b6b3-5d9001b1d901")
	)
	(junction
		(at 67.31 120.65)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "31df77ec-6f38-4722-80d7-1ada4325c076")
	)
	(junction
		(at 90.17 54.61)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "3637f19f-e642-44c9-af1f-08e6907a2fcb")
	)
	(junction
		(at 59.69 121.92)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "3e013cdd-07b0-4c74-ac82-4fef947c8752")
	)
	(junction
		(at 52.07 124.46)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "45e202f6-8e9d-4b86-87da-ef0db774bc74")
	)
	(junction
		(at 256.54 33.02)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "4618c4f5-39a3-47b8-afd8-51e9647e0ef6")
	)
	(junction
		(at 124.46 119.38)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "5661b342-ad55-45d3-af18-220ae37a631c")
	)
	(junction
		(at 196.85 182.88)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "60687886-cdb5-4472-b58a-6fa35b3d691a")
	)
	(junction
		(at 118.11 59.69)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "61037ca2-d487-415d-83a4-ad3d3f2ee38c")
	)
	(junction
		(at 73.66 208.28)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "6126c30d-717d-4ffe-adf0-390461e86b5b")
	)
	(junction
		(at 120.65 160.02)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "628b6108-e808-4283-838d-4e27965332ba")
	)
	(junction
		(at 149.86 143.51)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "6443d762-1182-40e0-afbc-ddff66aa83ba")
	)
	(junction
		(at 52.07 110.49)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "6963164f-0c28-4b89-bb2f-5a8162cd4d32")
	)
	(junction
		(at 68.58 208.28)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "6a5d48fb-af62-4114-850e-c5e9ec3249ee")
	)
	(junction
		(at 204.47 223.52)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "6f14ea0e-d357-491d-9986-9bbcc2119372")
	)
	(junction
		(at 204.47 210.82)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "7173eaf3-966d-496b-a93a-f711eb930dba")
	)
	(junction
		(at 35.56 173.99)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "7a0ba1a0-1e19-4e14-8f87-8f8d4c1c294d")
	)
	(junction
		(at 109.22 59.69)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "7e7635ac-af51-476b-993c-b8518f775c1a")
	)
	(junction
		(at 237.49 176.53)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "817b50af-4553-43d8-8b23-cb8532496ee8")
	)
	(junction
		(at 59.69 110.49)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "8d5ad07d-4def-41b8-8ba8-2007776f8954")
	)
	(junction
		(at 259.08 41.91)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "8f973d63-296d-4404-94a3-f7697e1d3f52")
	)
	(junction
		(at 59.69 124.46)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "91ba633e-3c55-4d84-8c22-b86ed4b073ea")
	)
	(junction
		(at 68.58 191.77)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "979e249f-cb73-4e2d-a18f-da5d5a09f5e9")
	)
	(junction
		(at 186.69 140.97)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "9f91aded-40ee-4aac-98ae-99d3b3606c37")
	)
	(junction
		(at 204.47 198.12)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "a0625edb-7110-4761-9a9e-2478ac21e5e8")
	)
	(junction
		(at 101.6 121.92)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "a078297d-cbec-457e-b2b1-bce8b813dc5b")
	)
	(junction
		(at 201.93 40.64)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "a6f02972-18d8-4463-b6ad-cae2bd005944")
	)
	(junction
		(at 240.03 39.37)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "a77ffd5f-763b-4880-ad90-412fdd2575fc")
	)
	(junction
		(at 238.76 72.39)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "be96c6b9-fb66-4ad3-84c0-1be2b65a14dd")
	)
	(junction
		(at 173.99 45.72)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "c4fab635-deca-41c9-bac4-6bdede3b1b58")
	)
	(junction
		(at 59.69 134.62)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "c5477387-5390-4ce9-aa5c-31f2560dd98c")
	)
	(junction
		(at 223.52 44.45)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "c8c92296-0b1d-46f3-8ad0-43fb381573f6")
	)
	(junction
		(at 100.33 67.31)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "d7442a7b-8797-4ca2-aff4-f27e65164f70")
	)
	(junction
		(at 76.2 171.45)
		(diameter 0)
		(color 0 0 0 0)
		(uuid "f325ba17-1fae-460a-b80b-a40b67ef4f72")
	)
	(no_connect
		(at 105.41 184.15)
		(uuid "908bb5c9-7fde-419e-b5d7-7585765e6507")
	)
	(no_connect
		(at 105.41 181.61)
		(uuid "ccfcec9b-7d84-45ac-9af7-ed594426e4ca")
	)
	(wire
		(pts
			(xy 165.1 138.43) (xy 165.1 127)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "0055013a-4ef5-49e2-aa4a-f3503a25d5ca")
	)
	(wire
		(pts
			(xy 248.92 83.82) (xy 255.27 83.82)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "018a0d8a-35c6-4b4c-943a-37fd42c47dfe")
	)
	(wire
		(pts
			(xy 275.59 214.63) (xy 279.4 214.63)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "02e0695c-9d70-454e-8676-810df1638c12")
	)
	(polyline
		(pts
			(xy 17.78 214.63) (xy 137.16 214.63)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "030f048a-5de8-4e4d-852a-96ec1b85e967")
	)
	(wire
		(pts
			(xy 52.07 124.46) (xy 52.07 120.65)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "039e109a-98d5-44ba-bc37-606dce1ce7e9")
	)
	(wire
		(pts
			(xy 35.56 182.88) (xy 35.56 185.42)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "0526a640-535e-4afb-af75-eb81b7871d47")
	)
	(wire
		(pts
			(xy 204.47 210.82) (xy 215.9 210.82)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "068148d8-5bcc-4719-9fba-7a781e689f3b")
	)
	(wire
		(pts
			(xy 55.88 238.76) (xy 60.96 238.76)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "06e3660c-53e7-44ef-a55f-55e293ed7298")
	)
	(polyline
		(pts
			(xy 100.33 229.87) (xy 19.05 229.87)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "08a11959-0fba-4f2a-9292-fadc6633204a")
	)
	(wire
		(pts
			(xy 104.14 109.22) (xy 124.46 109.22)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "0a048246-70ff-44c0-80d3-3bf72eff8383")
	)
	(wire
		(pts
			(xy 99.06 179.07) (xy 105.41 179.07)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "0c775b7c-7397-4b65-bf60-7a045a1934b1")
	)
	(wire
		(pts
			(xy 165.1 127) (xy 186.69 127)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "0e16e700-bb92-4501-8a2a-cc54c41b8c13")
	)
	(wire
		(pts
			(xy 43.18 154.94) (xy 35.56 158.75)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "0f44c850-e2f1-4973-a1f1-e8008fc150c0")
	)
	(wire
		(pts
			(xy 96.52 173.99) (xy 105.41 173.99)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "124cf54f-56d8-455b-843e-42f6b8012ebc")
	)
	(wire
		(pts
			(xy 63.5 208.28) (xy 68.58 208.28)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "1382a857-c7d5-4374-a464-df058d714c71")
	)
	(wire
		(pts
			(xy 149.86 143.51) (xy 168.91 143.51)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "15ad8538-38a8-4d9d-ad16-eeb3d3cfec3d")
	)
	(wire
		(pts
			(xy 223.52 54.61) (xy 223.52 57.15)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "16530cb4-c612-404a-a656-277eb5851c15")
	)
	(wire
		(pts
			(xy 237.49 176.53) (xy 279.4 176.53)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "165866b8-c2d8-4acc-a161-8c718d540709")
	)
	(wire
		(pts
			(xy 256.54 33.02) (xy 259.08 33.02)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "16f0b52b-61ff-47b5-91dc-b3d7c7e3b5ca")
	)
	(wire
		(pts
			(xy 189.23 40.64) (xy 189.23 33.02)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "199eb112-e5aa-478a-90b2-a3ed934c4c02")
	)
	(wire
		(pts
			(xy 33.02 66.04) (xy 43.18 66.04)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "19b85bbb-9588-4211-804b-84829d511da2")
	)
	(wire
		(pts
			(xy 111.76 111.76) (xy 111.76 105.41)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "19bd768a-3b21-4f14-a3f7-7aa2a7e70fd6")
	)
	(polyline
		(pts
			(xy 71.12 29.21) (xy 71.12 85.09)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "1ba9cb20-b71b-47b9-8673-01000c1166e8")
	)
	(wire
		(pts
			(xy 223.52 210.82) (xy 256.54 210.82)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "1fa74535-d545-4878-9102-918b605c2869")
	)
	(wire
		(pts
			(xy 173.99 152.4) (xy 173.99 148.59)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "2060e7b7-6e4b-4431-9a9f-17483342328a")
	)
	(wire
		(pts
			(xy 90.17 54.61) (xy 90.17 55.88)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "213ea3a1-5684-41b3-908d-05a5b5441f3b")
	)
	(wire
		(pts
			(xy 204.47 198.12) (xy 204.47 210.82)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "21d43ecd-15a7-48ac-9b2f-5b63c05ba5d5")
	)
	(wire
		(pts
			(xy 264.16 201.93) (xy 264.16 205.74)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "239e746e-2989-413a-99f7-97f7de119683")
	)
	(wire
		(pts
			(xy 181.61 35.56) (xy 201.93 35.56)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "25ba5e66-1b8d-4f0c-9f2d-c1a5fc96e698")
	)
	(polyline
		(pts
			(xy 186.69 246.38) (xy 290.83 246.38)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "2660ab68-63c0-4192-aa22-83e301b9ca68")
	)
	(wire
		(pts
			(xy 33.02 59.69) (xy 43.18 59.69)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "26969d48-0b1a-4c4e-af13-7072e0c16b40")
	)
	(wire
		(pts
			(xy 255.27 83.82) (xy 255.27 69.85)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "26e13bd3-f0a8-4ecb-ba33-ae1c952ad530")
	)
	(wire
		(pts
			(xy 106.68 116.84) (xy 104.14 116.84)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "289e36f6-d369-4600-9f5e-cf5e0e6fac56")
	)
	(wire
		(pts
			(xy 59.69 134.62) (xy 59.69 137.16)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "28d011fe-5bdf-4713-8a2b-7e606fdfcfb1")
	)
	(wire
		(pts
			(xy 153.67 184.15) (xy 130.81 184.15)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "2914cca9-7307-4163-800b-a30cf7f0ca2b")
	)
	(wire
		(pts
			(xy 186.69 127) (xy 186.69 140.97)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "2cfea895-5a8a-4bb3-8730-706eb68d9db4")
	)
	(wire
		(pts
			(xy 86.36 191.77) (xy 68.58 191.77)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "2da47cf7-8dc1-4579-b64d-1c7ecf457716")
	)
	(wire
		(pts
			(xy 59.69 110.49) (xy 67.31 110.49)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "2e8cfc04-832b-41f9-bbc8-c6da1dc05107")
	)
	(wire
		(pts
			(xy 50.8 154.94) (xy 59.69 158.75)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "2ee5cb7b-3b69-468c-9ec7-7c6c36350fb7")
	)
	(wire
		(pts
			(xy 237.49 176.53) (xy 237.49 172.72)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "2fa98ce9-1c73-4ec6-96c3-b1fca136a640")
	)
	(wire
		(pts
			(xy 31.75 168.91) (xy 35.56 168.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "301be842-6529-47ca-b691-828c490198c6")
	)
	(wire
		(pts
			(xy 59.69 113.03) (xy 59.69 110.49)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "30eddf20-bd0f-4fb1-844e-392021977946")
	)
	(polyline
		(pts
			(xy 129.54 78.74) (xy 129.54 36.83)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "3178f8d1-829e-4752-a1c1-61e8669c5ef3")
	)
	(wire
		(pts
			(xy 69.85 171.45) (xy 76.2 171.45)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "327564ac-60da-4e82-9bc3-d12605128b5c")
	)
	(wire
		(pts
			(xy 99.06 180.34) (xy 99.06 179.07)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "34248d3a-e9c6-41df-94c6-3ea960103d7b")
	)
	(wire
		(pts
			(xy 118.11 160.02) (xy 120.65 160.02)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "352687dc-3271-4dc1-bec9-22dbbfe42b0e")
	)
	(wire
		(pts
			(xy 67.31 124.46) (xy 67.31 120.65)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "36d8d10e-2290-4468-98a6-abe9b9715dde")
	)
	(wire
		(pts
			(xy 215.9 40.64) (xy 201.93 40.64)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "38a62a3a-adf5-4b86-a1af-3b67f7d7f183")
	)
	(wire
		(pts
			(xy 81.28 194.31) (xy 105.41 194.31)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "3a4675b4-4d77-42f1-8720-b13f72221a3e")
	)
	(wire
		(pts
			(xy 223.52 182.88) (xy 229.87 182.88)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "3c932012-d243-4e03-bf6d-436043dd99f1")
	)
	(wire
		(pts
			(xy 52.07 124.46) (xy 59.69 124.46)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "3d005b57-e603-498b-8b5e-4dafbc5d33d5")
	)
	(wire
		(pts
			(xy 105.41 176.53) (xy 102.87 176.53)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "3edee389-17b5-49f3-9e3a-58c71f71ba09")
	)
	(polyline
		(pts
			(xy 27.94 29.21) (xy 27.94 85.09)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "3faf2f7d-ee71-4b4d-b791-18bd432f8378")
	)
	(wire
		(pts
			(xy 104.14 116.84) (xy 104.14 109.22)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "41de43e6-cd01-4fee-973a-ef0495be6f5a")
	)
	(polyline
		(pts
			(xy 137.16 214.63) (xy 137.16 151.13)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "424f296d-e014-4840-a554-7c696b1bc2b0")
	)
	(wire
		(pts
			(xy 59.69 132.08) (xy 59.69 134.62)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "4614e04e-c865-416b-8c43-84550155969b")
	)
	(wire
		(pts
			(xy 259.08 41.91) (xy 273.05 41.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "47d4f996-6fbe-4482-bd2c-12a3d7703242")
	)
	(wire
		(pts
			(xy 215.9 198.12) (xy 204.47 198.12)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "47f65b3c-ac3b-41fc-8646-a4a23b6cd504")
	)
	(wire
		(pts
			(xy 240.03 44.45) (xy 223.52 44.45)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "4876c43a-933a-4593-93da-fc199215c184")
	)
	(polyline
		(pts
			(xy 62.23 85.09) (xy 71.12 85.09)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "4906b367-f737-451f-b3ee-b7dcaa6a6287")
	)
	(wire
		(pts
			(xy 67.31 124.46) (xy 90.17 124.46)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "4c073915-495b-439e-a686-e7ef986a93f6")
	)
	(wire
		(pts
			(xy 118.11 54.61) (xy 118.11 59.69)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "4c7346c4-0bef-4fc5-ac40-786f4f373fcb")
	)
	(wire
		(pts
			(xy 120.65 160.02) (xy 120.65 163.83)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "4ff8352b-161c-4b6b-8591-171e7a668d45")
	)
	(wire
		(pts
			(xy 116.84 157.48) (xy 105.41 157.48)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "53799c00-6f2f-4238-87ce-e20f1e2559a4")
	)
	(wire
		(pts
			(xy 279.4 191.77) (xy 250.19 191.77)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "54e2d3e8-a010-4d65-afc0-a0ae108d1e96")
	)
	(wire
		(pts
			(xy 63.5 186.69) (xy 72.39 186.69)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "56cddd4a-2a22-429d-8d38-035efd443483")
	)
	(polyline
		(pts
			(xy 186.69 157.48) (xy 186.69 246.38)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "596af2e2-e6f9-4575-987e-f63905504300")
	)
	(wire
		(pts
			(xy 196.85 182.88) (xy 204.47 182.88)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "5ad2690e-d406-4871-a271-55b940d7287d")
	)
	(wire
		(pts
			(xy 105.41 189.23) (xy 68.58 189.23)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "5d562f5b-1333-4ce1-9e45-93283559a1ca")
	)
	(wire
		(pts
			(xy 102.87 59.69) (xy 109.22 59.69)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "5f220553-953f-48a7-ae47-361b86ef25ef")
	)
	(wire
		(pts
			(xy 251.46 29.21) (xy 256.54 29.21)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "611c106c-d351-4dc3-8288-2b621ce6a8f1")
	)
	(wire
		(pts
			(xy 35.56 173.99) (xy 39.37 173.99)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "6165a315-2911-4c05-8db8-76ae23c8f477")
	)
	(wire
		(pts
			(xy 205.74 72.39) (xy 228.6 72.39)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "636776e2-190c-4ded-9c7c-75635691407e")
	)
	(polyline
		(pts
			(xy 290.83 157.48) (xy 186.69 157.48)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "64825ea7-2258-4372-bc64-3ba29a65449c")
	)
	(wire
		(pts
			(xy 149.86 139.7) (xy 149.86 143.51)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "65f3cd36-c525-4e14-a315-a86a2810f11e")
	)
	(wire
		(pts
			(xy 241.3 83.82) (xy 238.76 83.82)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "665aacc7-dcbf-42aa-beb5-20e067134017")
	)
	(wire
		(pts
			(xy 259.08 33.02) (xy 259.08 41.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "6814db72-d79d-4eca-8567-738a6394577d")
	)
	(wire
		(pts
			(xy 240.03 67.31) (xy 231.14 67.31)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "6889f26c-7749-49a3-9314-b0a288b37f9e")
	)
	(polyline
		(pts
			(xy 140.97 144.78) (xy 27.94 144.78)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "6a0bfb88-1c0b-4869-9253-4527ed4dc8e4")
	)
	(wire
		(pts
			(xy 201.93 35.56) (xy 201.93 40.64)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "6a8d6ac7-e092-4edc-a781-469ad0edf4b9")
	)
	(wire
		(pts
			(xy 186.69 140.97) (xy 184.15 140.97)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "6acc80ae-1d11-4baf-b44c-766df474d8b6")
	)
	(wire
		(pts
			(xy 76.2 120.65) (xy 67.31 120.65)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "6caa4381-4ae9-4d33-a7fd-af52ab22ab84")
	)
	(wire
		(pts
			(xy 102.87 176.53) (xy 102.87 143.51)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "6e8a7163-da33-451a-9324-599d876a3fb4")
	)
	(wire
		(pts
			(xy 204.47 237.49) (xy 204.47 240.03)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "72c27581-8d73-4539-abdc-7b5e68813a23")
	)
	(wire
		(pts
			(xy 68.58 208.28) (xy 73.66 208.28)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "73732222-bf0e-4fcf-b09c-27d7ff14c43a")
	)
	(polyline
		(pts
			(xy 81.28 78.74) (xy 129.54 78.74)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "7457b41d-6835-44c1-b93f-f2b798d233eb")
	)
	(wire
		(pts
			(xy 88.9 173.99) (xy 86.36 173.99)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "749306e3-0c7d-4695-be11-e7c678c8cd17")
	)
	(wire
		(pts
			(xy 48.26 238.76) (xy 44.45 238.76)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "74dd3f96-98ee-4ec6-9ba7-e10a2ccfea31")
	)
	(wire
		(pts
			(xy 68.58 189.23) (xy 68.58 191.77)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "78028834-e29c-40b2-8e74-49481ac3caaa")
	)
	(wire
		(pts
			(xy 100.33 67.31) (xy 109.22 67.31)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "786a3959-921d-40fc-9e5a-2c4f87eb7619")
	)
	(polyline
		(pts
			(xy 137.16 151.13) (xy 16.51 151.13)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "78933c81-2bad-4e33-a2b5-942cdcdaf5e4")
	)
	(wire
		(pts
			(xy 240.03 33.02) (xy 240.03 39.37)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "789e5ec4-ce19-4f18-8b0a-d959015e4aa2")
	)
	(polyline
		(pts
			(xy 17.78 214.63) (xy 137.16 214.63)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "7cf62219-ffde-4e7b-87d7-0419a33281aa")
	)
	(wire
		(pts
			(xy 76.2 171.45) (xy 105.41 171.45)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "7d1a58ce-2923-4718-aa96-0f36c6adacb6")
	)
	(wire
		(pts
			(xy 67.31 134.62) (xy 67.31 132.08)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "7d53ec36-36b1-42f3-91fb-a78789f468e5")
	)
	(polyline
		(pts
			(xy 147.32 20.32) (xy 275.59 20.32)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "7d712e7d-811f-443f-a3c5-5c09b3e5207d")
	)
	(wire
		(pts
			(xy 275.59 214.63) (xy 275.59 218.44)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "7de2b1d1-ad62-4431-80dc-aa9aba02293d")
	)
	(wire
		(pts
			(xy 250.19 191.77) (xy 250.19 193.04)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "7e3d7300-e9c1-4111-9326-183262d0e041")
	)
	(wire
		(pts
			(xy 223.52 223.52) (xy 267.97 223.52)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "7e4bb0ff-5ff0-4085-aa57-2b4740101fe9")
	)
	(wire
		(pts
			(xy 100.33 121.92) (xy 101.6 121.92)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "7f98f95e-9506-467c-9811-7d7000389f94")
	)
	(wire
		(pts
			(xy 101.6 121.92) (xy 106.68 121.92)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "802b63d9-6be5-4f15-a17d-2a57297140d9")
	)
	(wire
		(pts
			(xy 237.49 165.1) (xy 237.49 162.56)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "80742f9c-d3bd-4f51-b73d-cf92d994adb0")
	)
	(polyline
		(pts
			(xy 62.23 29.21) (xy 27.94 29.21)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "83079048-172d-4b53-b920-2c1f7df588f5")
	)
	(wire
		(pts
			(xy 173.99 35.56) (xy 173.99 45.72)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "83bafac3-55db-4e95-af92-74073160bcdb")
	)
	(polyline
		(pts
			(xy 290.83 246.38) (xy 290.83 157.48)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "86b8bf2d-47c3-4a3d-b45c-1aa83e9d55ba")
	)
	(wire
		(pts
			(xy 102.87 43.18) (xy 102.87 49.53)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "87fdbece-38c3-47ba-87eb-9b0c82232c28")
	)
	(wire
		(pts
			(xy 31.75 173.99) (xy 35.56 173.99)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "884e662f-2f3a-4447-8e37-b8280f44937b")
	)
	(wire
		(pts
			(xy 124.46 109.22) (xy 124.46 119.38)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "88870e0a-0ae0-4303-9030-e9b9a52aab57")
	)
	(polyline
		(pts
			(xy 100.33 261.62) (xy 100.33 229.87)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "8a4ee2b3-8b9a-49ed-8af7-d9876c679e8d")
	)
	(wire
		(pts
			(xy 279.4 201.93) (xy 264.16 201.93)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "8be3707f-dcc8-4b88-a0f8-c142516cee2c")
	)
	(wire
		(pts
			(xy 196.85 179.07) (xy 196.85 182.88)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "8e27541f-8f25-4d5c-87a5-30a0c77cb890")
	)
	(wire
		(pts
			(xy 109.22 59.69) (xy 118.11 59.69)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "8f470134-0fd9-46d1-b74d-b0faa6b246a2")
	)
	(wire
		(pts
			(xy 124.46 119.38) (xy 132.08 119.38)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "91724e89-b1cf-4ebb-a1d7-c38256e54b82")
	)
	(wire
		(pts
			(xy 255.27 41.91) (xy 259.08 41.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "98791a23-29fd-4c5c-a6cf-c5377f4f8c56")
	)
	(wire
		(pts
			(xy 59.69 158.75) (xy 59.69 171.45)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "98892e87-580c-4922-a20f-b871cea817ef")
	)
	(polyline
		(pts
			(xy 62.23 29.21) (xy 71.12 29.21)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "98ef0909-7761-47b3-a792-9d4a514a93a9")
	)
	(polyline
		(pts
			(xy 275.59 88.9) (xy 147.32 88.9)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "995be463-1db4-463c-91b6-fe63c2549738")
	)
	(wire
		(pts
			(xy 237.49 176.53) (xy 237.49 177.8)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "99c39a85-00a7-483c-87c3-6d5693539151")
	)
	(wire
		(pts
			(xy 201.93 40.64) (xy 201.93 48.26)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "99cbd133-b0c7-4773-b5da-2a27669414af")
	)
	(wire
		(pts
			(xy 105.41 191.77) (xy 93.98 191.77)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "9af76fc2-b41f-4873-baa3-2ea339231254")
	)
	(wire
		(pts
			(xy 78.74 173.99) (xy 76.2 173.99)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "9f59abcf-af8e-4bea-b070-f1d8c6928d5a")
	)
	(wire
		(pts
			(xy 68.58 191.77) (xy 68.58 196.85)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "a04ac421-8c55-4467-8e8e-829246d65504")
	)
	(wire
		(pts
			(xy 102.87 143.51) (xy 149.86 143.51)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "a0a2b35e-52dd-4b0a-bbde-36f5ddbdb837")
	)
	(wire
		(pts
			(xy 67.31 113.03) (xy 67.31 110.49)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "a25abb0b-f587-4cca-945f-0dd0c57300f2")
	)
	(wire
		(pts
			(xy 196.85 179.07) (xy 194.31 179.07)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "a27bf3c9-c4b5-43bc-895d-32276772a5b3")
	)
	(wire
		(pts
			(xy 59.69 121.92) (xy 59.69 124.46)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "a44f45fd-35c6-4cd5-8ca7-7ebb6713d9dd")
	)
	(wire
		(pts
			(xy 59.69 134.62) (xy 67.31 134.62)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "a5cf3cd9-95d5-4657-bb3c-6a8c3465cc4f")
	)
	(wire
		(pts
			(xy 81.28 200.66) (xy 81.28 194.31)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "a8cc7ad7-08b6-4f06-bd7f-b9adcf5c62eb")
	)
	(polyline
		(pts
			(xy 129.54 36.83) (xy 81.28 36.83)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "a9f3ce58-8891-48e4-8242-3abf4c79ea85")
	)
	(wire
		(pts
			(xy 76.2 119.38) (xy 76.2 120.65)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "aa0fe979-9a8d-42d9-8941-3a6e088d6769")
	)
	(wire
		(pts
			(xy 39.37 250.19) (xy 48.26 250.19)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "aa43a3e0-004d-4abd-975f-3fe470b63543")
	)
	(wire
		(pts
			(xy 95.25 54.61) (xy 90.17 54.61)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "aa48c382-84f7-42b4-9ba9-e40927023398")
	)
	(wire
		(pts
			(xy 63.5 186.69) (xy 63.5 208.28)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ab21b816-bb24-400d-81d0-c35cede239d4")
	)
	(wire
		(pts
			(xy 46.99 78.74) (xy 30.48 78.74)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ab8d3567-0611-422a-a8e6-5d6b8a6a9b63")
	)
	(wire
		(pts
			(xy 153.67 190.5) (xy 186.69 190.5)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ac557484-5c4a-4fc3-9a33-0a1568abd29b")
	)
	(wire
		(pts
			(xy 46.99 73.66) (xy 31.75 73.66)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ae9626c9-eab7-4762-aace-e67b6574ebd9")
	)
	(wire
		(pts
			(xy 52.07 134.62) (xy 59.69 134.62)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "af19e875-a5c7-4312-9a02-c06aa74776d2")
	)
	(wire
		(pts
			(xy 35.56 173.99) (xy 35.56 175.26)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "af79f6ee-19a9-4d9a-88ea-4131d2756dff")
	)
	(wire
		(pts
			(xy 186.69 140.97) (xy 193.04 134.62)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b041805d-ff0e-4d5d-8b38-a7d94fd14dd1")
	)
	(polyline
		(pts
			(xy 19.05 261.62) (xy 100.33 261.62)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "b16f28d6-a069-44dc-ac79-f999e293dbae")
	)
	(polyline
		(pts
			(xy 81.28 36.83) (xy 81.28 78.74)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "b3e38d2e-d652-4606-9bcc-e7674345fa60")
	)
	(wire
		(pts
			(xy 238.76 72.39) (xy 240.03 72.39)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b410435a-a6c2-4c56-9748-4263c1ee17a8")
	)
	(wire
		(pts
			(xy 35.56 158.75) (xy 35.56 168.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b4c69fe8-4dcf-497b-882c-4307b8bf8027")
	)
	(polyline
		(pts
			(xy 27.94 99.06) (xy 140.97 99.06)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "b53570a7-670d-4537-a831-cfbeab27fe49")
	)
	(wire
		(pts
			(xy 105.41 157.48) (xy 105.41 168.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b5670da9-16cc-4c05-b93c-b2db10a8c6d4")
	)
	(wire
		(pts
			(xy 35.56 168.91) (xy 39.37 168.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b789f255-c4e0-42ca-b981-39f3b0e4a008")
	)
	(wire
		(pts
			(xy 90.17 43.18) (xy 102.87 43.18)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b79d4dab-978a-44ce-9f4f-f93ee400b580")
	)
	(wire
		(pts
			(xy 223.52 198.12) (xy 242.57 198.12)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b7b3582f-6947-4fb4-a8ed-bfc51104edeb")
	)
	(wire
		(pts
			(xy 54.61 171.45) (xy 59.69 171.45)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b9130e77-4548-44dc-88da-2bf90ede7e59")
	)
	(wire
		(pts
			(xy 59.69 121.92) (xy 90.17 121.92)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "b9459bb9-a86f-4cb8-b0cb-9fd6b4cf5e9f")
	)
	(wire
		(pts
			(xy 118.11 59.69) (xy 119.38 59.69)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ba0c3d71-b4fe-4b37-ac16-5ad3331d4407")
	)
	(wire
		(pts
			(xy 35.56 185.42) (xy 41.91 185.42)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "bba5c499-3d6a-4d90-9bc8-7929313eea8d")
	)
	(wire
		(pts
			(xy 223.52 31.75) (xy 228.6 31.75)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "bc6b219c-83e5-475a-8629-080da7423ee7")
	)
	(wire
		(pts
			(xy 68.58 238.76) (xy 80.01 238.76)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "bca123f6-22b4-40e8-8bb3-f0dc49ed71ef")
	)
	(wire
		(pts
			(xy 52.07 132.08) (xy 52.07 134.62)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "bcbc8476-72e9-41e8-b97d-9224735a76e1")
	)
	(polyline
		(pts
			(xy 16.51 151.13) (xy 16.51 214.63)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "bd6d168a-4e1c-4ce5-8d4e-7e95200dd140")
	)
	(wire
		(pts
			(xy 90.17 63.5) (xy 90.17 67.31)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "be829644-30a6-4962-8b1c-fe407029ecb5")
	)
	(polyline
		(pts
			(xy 275.59 20.32) (xy 275.59 88.9)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "bf13f7b6-fcfa-48a7-bf76-5f19617dd7a3")
	)
	(polyline
		(pts
			(xy 27.94 144.78) (xy 27.94 99.06)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "bfdb1cb7-0036-4585-bd75-867e65d5ba3f")
	)
	(wire
		(pts
			(xy 118.11 163.83) (xy 118.11 160.02)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "c2d4b8d0-93b9-4167-9ba5-81e662fd6e72")
	)
	(wire
		(pts
			(xy 76.2 119.38) (xy 90.17 119.38)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "c5ba60b9-3aee-4825-b8ea-dcd931561c7b")
	)
	(wire
		(pts
			(xy 223.52 36.83) (xy 223.52 31.75)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "c60e5866-a234-4d53-94e8-b6d5d08f9d39")
	)
	(wire
		(pts
			(xy 76.2 173.99) (xy 76.2 171.45)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "c6820d33-d5fb-4a9e-9f1f-07e718d4d3d6")
	)
	(wire
		(pts
			(xy 153.67 190.5) (xy 153.67 184.15)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "c6c127f8-e8f9-4268-af5f-c87ea43897bd")
	)
	(wire
		(pts
			(xy 59.69 120.65) (xy 59.69 121.92)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "c74b15ce-4133-4f77-b207-5cacc83a907a")
	)
	(wire
		(pts
			(xy 90.17 67.31) (xy 100.33 67.31)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "c9a73e1e-179d-41a0-bee1-b6b659c811ef")
	)
	(wire
		(pts
			(xy 30.48 110.49) (xy 52.07 110.49)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "cd4e4536-5904-43dd-8830-006df412df96")
	)
	(wire
		(pts
			(xy 238.76 83.82) (xy 238.76 72.39)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ce8d7ea5-b4ea-44a6-b0a8-952955c0134a")
	)
	(wire
		(pts
			(xy 204.47 182.88) (xy 215.9 182.88)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "cea1311c-e90e-4b4e-879e-665ad318613f")
	)
	(wire
		(pts
			(xy 52.07 110.49) (xy 59.69 110.49)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "cf661e2e-df45-4f57-836f-4da7f0358a9a")
	)
	(wire
		(pts
			(xy 90.17 43.18) (xy 90.17 45.72)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "cfddd6ba-4934-47b7-8bb8-41233b7b294d")
	)
	(wire
		(pts
			(xy 223.52 44.45) (xy 223.52 46.99)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d04180b1-7976-4a37-8183-1411ed34b0c6")
	)
	(wire
		(pts
			(xy 173.99 45.72) (xy 184.15 45.72)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d1217efc-a7b6-4e96-9621-d0e414c87385")
	)
	(wire
		(pts
			(xy 204.47 223.52) (xy 215.9 223.52)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d1ee1028-29ad-4d02-8ea2-a377d21e8bff")
	)
	(wire
		(pts
			(xy 43.18 48.26) (xy 35.56 48.26)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d5068560-5ddc-4a5e-9083-36924c32b31d")
	)
	(polyline
		(pts
			(xy 147.32 88.9) (xy 147.32 20.32)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "d59d0981-f669-4d7a-b7fb-57d16134cbe0")
	)
	(wire
		(pts
			(xy 24.13 168.91) (xy 17.78 168.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d6931357-3310-4c1b-8387-bf81ad61f650")
	)
	(wire
		(pts
			(xy 204.47 210.82) (xy 204.47 223.52)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d75c8419-5dc9-458e-9d77-589926927a81")
	)
	(wire
		(pts
			(xy 201.93 48.26) (xy 199.39 48.26)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d7d10ff7-dcda-457a-9bda-9fd3b8b1b950")
	)
	(wire
		(pts
			(xy 255.27 33.02) (xy 256.54 33.02)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d7ff5b21-5a56-45ac-b7c4-75c6ddd98369")
	)
	(wire
		(pts
			(xy 35.56 48.26) (xy 35.56 49.53)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d8382140-628f-49d3-8d41-d6cdc7aef20f")
	)
	(wire
		(pts
			(xy 35.56 41.91) (xy 43.18 41.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "d9d59f50-6624-4288-872f-d65eaa2a5868")
	)
	(wire
		(pts
			(xy 255.27 69.85) (xy 260.35 69.85)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "dab21bc6-b487-45ee-b417-c6b905028ae5")
	)
	(wire
		(pts
			(xy 236.22 72.39) (xy 238.76 72.39)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "db7e6ead-fb0e-424d-962d-9b525bfb3f3b")
	)
	(wire
		(pts
			(xy 55.88 250.19) (xy 60.96 250.19)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "dc8408e3-968c-446c-a4dc-b49f2548f099")
	)
	(wire
		(pts
			(xy 256.54 29.21) (xy 256.54 33.02)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "dddfc451-08f9-49e6-82a8-81806153a7e3")
	)
	(wire
		(pts
			(xy 68.58 208.28) (xy 68.58 204.47)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "de6bb20a-fecd-4c02-b7c0-448048a39883")
	)
	(wire
		(pts
			(xy 204.47 182.88) (xy 204.47 198.12)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "de7d0b8e-6b2e-4ede-bf6b-9d0219760d3f")
	)
	(polyline
		(pts
			(xy 62.23 85.09) (xy 27.94 85.09)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "df3b6866-6313-4204-8e2b-fbd588f8dd5a")
	)
	(wire
		(pts
			(xy 24.13 173.99) (xy 17.78 173.99)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "e02c6f9f-b140-4219-9cfa-787bd8d65f7c")
	)
	(wire
		(pts
			(xy 59.69 124.46) (xy 67.31 124.46)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "e0636cf8-8ad9-4e37-915b-0fa98a926718")
	)
	(wire
		(pts
			(xy 196.85 190.5) (xy 194.31 190.5)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "e2b1a971-f36d-4001-a679-4930c9178f1f")
	)
	(wire
		(pts
			(xy 247.65 33.02) (xy 240.03 33.02)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "e4c72a32-bf21-40af-81e4-b58475befbc8")
	)
	(wire
		(pts
			(xy 130.81 179.07) (xy 186.69 179.07)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "e5010d90-f5c6-4102-a878-cf3ec7bbdf35")
	)
	(wire
		(pts
			(xy 196.85 182.88) (xy 196.85 190.5)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ea5676bd-1fc0-4ee6-91d1-4d94dd8871e9")
	)
	(wire
		(pts
			(xy 30.48 76.2) (xy 46.99 76.2)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "eb1abf22-9ce3-4537-be4d-2bee8ce35f66")
	)
	(wire
		(pts
			(xy 35.56 34.29) (xy 35.56 41.91)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ec91d857-46bb-4e6e-a2b0-f88a0c34dab0")
	)
	(polyline
		(pts
			(xy 19.05 229.87) (xy 19.05 261.62)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "ec96a201-355f-4b7b-9ace-d658c2040314")
	)
	(wire
		(pts
			(xy 52.07 113.03) (xy 52.07 110.49)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "eddbbe30-e88e-4b2e-aa50-284381d1e5a5")
	)
	(wire
		(pts
			(xy 68.58 250.19) (xy 80.01 250.19)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ee6aa324-7a58-4166-ba76-30b39c1ee352")
	)
	(wire
		(pts
			(xy 204.47 223.52) (xy 204.47 229.87)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "eef5bda7-4ffc-4f22-927c-340c48883590")
	)
	(wire
		(pts
			(xy 189.23 55.88) (xy 189.23 57.15)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f14777a0-6e2e-41dc-a141-cafe0beafa49")
	)
	(wire
		(pts
			(xy 73.66 208.28) (xy 81.28 208.28)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f1670817-cb38-4029-bb4a-3ff7b763fe02")
	)
	(wire
		(pts
			(xy 245.11 49.53) (xy 245.11 55.88)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f3b94d5e-a4e7-4c0a-ae15-3cbc0d971507")
	)
	(wire
		(pts
			(xy 245.11 31.75) (xy 245.11 34.29)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f41109f2-6a41-4a39-982e-2bb0beff5628")
	)
	(wire
		(pts
			(xy 189.23 57.15) (xy 200.66 57.15)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f4ac4294-a7ce-4735-9d13-76730f169116")
	)
	(wire
		(pts
			(xy 90.17 53.34) (xy 90.17 54.61)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f518028c-efe7-4b5c-8090-9412a21475d5")
	)
	(wire
		(pts
			(xy 168.91 138.43) (xy 165.1 138.43)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f7105081-b868-4fd7-91e6-ab9606157bf9")
	)
	(wire
		(pts
			(xy 105.41 186.69) (xy 80.01 186.69)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f7de8a11-8a1b-4566-a7b3-5145cff58f14")
	)
	(wire
		(pts
			(xy 171.45 50.8) (xy 184.15 50.8)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f7f84a60-6813-473c-820d-fea1bd8f5f5c")
	)
	(wire
		(pts
			(xy 163.83 45.72) (xy 173.99 45.72)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "f83de86d-8728-4b3a-a237-6ec0e507c213")
	)
	(wire
		(pts
			(xy 59.69 171.45) (xy 62.23 171.45)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "faa00b26-69c3-4766-95c6-d465a1063658")
	)
	(wire
		(pts
			(xy 128.27 160.02) (xy 120.65 160.02)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "fb0e038b-70cd-448c-8dd9-ac378c9d25f6")
	)
	(polyline
		(pts
			(xy 140.97 99.06) (xy 140.97 144.78)
		)
		(stroke
			(width 0)
			(type dash)
		)
		(uuid "fcb22f62-db45-4a6a-bbef-f2c19c9088f6")
	)
	(wire
		(pts
			(xy 121.92 119.38) (xy 124.46 119.38)
		)
		(stroke
			(width 0)
			(type default)
		)
		(uuid "ffd2457b-e7c7-4b05-87ea-1d2c6aed8cb5")
	)
	(text "Connectors\n\n"
		(exclude_from_sim no)
		(at 50.8 30.48 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "176e796c-015f-4dd5-b866-10548351632d")
	)
	(text "Power supply\n\n\n"
		(exclude_from_sim no)
		(at 96.52 39.37 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "682abbcf-edf0-4f8d-9e4d-0e619233ce66")
	)
	(text "Sensor Interface\n"
		(exclude_from_sim no)
		(at 199.39 19.05 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "9e878584-aaf2-42fd-8790-1d2d2a60bbaf")
	)
	(text "Power interface\n"
		(exclude_from_sim no)
		(at 220.98 156.21 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "aa1cbb2c-a019-45eb-a3ef-d8f417268d52")
	)
	(text "Lightning\n"
		(exclude_from_sim no)
		(at 54.102 228.092 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "af081574-e5cb-49d8-b822-7f930f863f98")
	)
	(text "Proportional Integral controller\n"
		(exclude_from_sim no)
		(at 54.61 149.86 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "ee3c04bc-d659-4ad3-a944-77abb2a4c132")
	)
	(text "Battery voltage measurement interface\n"
		(exclude_from_sim no)
		(at 62.23 97.79 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "fb6d4493-431e-4ac3-a9fb-dfc52f7a4890")
	)
	(label "Sensor_VSS"
		(at 273.05 41.91 180)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify right bottom)
		)
		(uuid "02ed6702-ad4b-4f05-a306-523567975613")
	)
	(label "Vcons"
		(at 260.35 69.85 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "15ded8b3-c51a-4d2b-82b4-eb932d9906ed")
	)
	(label "Vmeas"
		(at 17.78 168.91 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "2277b917-2f57-4ec6-ae7c-7622a7e18642")
	)
	(label "Vref"
		(at 105.41 157.48 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "323c7ad0-1abe-48bc-93da-e7672516bf1b")
	)
	(label "Supply"
		(at 80.01 238.76 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "37aba8cd-c6e1-4fdd-a138-3fc2a17b240c")
	)
	(label "Vdump"
		(at 279.4 214.63 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "3ad4125c-3e4f-4aeb-8a04-5e4adce7dc13")
	)
	(label "Sensor_VDD"
		(at 215.9 40.64 180)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify right bottom)
		)
		(uuid "46894431-b965-408b-980b-eae49e1ec461")
	)
	(label "Vcons"
		(at 17.78 173.99 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "4b173fab-8f31-462d-8b57-a9619dabe4e1")
	)
	(label "Vref"
		(at 41.91 185.42 180)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify right bottom)
		)
		(uuid "5158e2be-a8ff-4124-ab25-2161dadd4fb6")
	)
	(label "12V"
		(at 83.82 124.46 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "5909e596-8289-420a-afe7-26d9c31b96d5")
	)
	(label "Vdump"
		(at 33.02 59.69 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "5d1516c4-6a8b-4af9-a032-3d3bfbfc947c")
	)
	(label "Supply"
		(at 128.27 160.02 180)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify right bottom)
		)
		(uuid "68e3f85a-b389-4b02-b6f0-05f35120e169")
	)
	(label "Vmeas"
		(at 132.08 119.38 180)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify right bottom)
		)
		(uuid "748358b0-e7df-4ece-982d-890f98557dfc")
	)
	(label "Vout_sensor"
		(at 30.48 76.2 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "78adf2e3-347a-4fed-bbc5-ac179f45a394")
	)
	(label "Supply"
		(at 200.66 57.15 180)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify right bottom)
		)
		(uuid "848d48cc-6c1c-4607-9c56-84e7d1f92115")
	)
	(label "Supply"
		(at 111.76 127 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "870d8963-418a-4ad9-89fc-b9dc1b43ca59")
	)
	(label "Vref"
		(at 193.04 134.62 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "8a920c62-1360-48d0-be8b-50cb592611cd")
	)
	(label "Sensor_VSS"
		(at 30.48 78.74 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "8d278d73-cb62-44ca-8209-b9c88c1be881")
	)
	(label "Vin"
		(at 80.01 250.19 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "8fa22c33-ffbe-443e-9aee-a036f78b8130")
	)
	(label "Vdump"
		(at 279.4 176.53 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "90b19b68-8d77-45f1-973f-c103b7de7216")
	)
	(label "Supply"
		(at 44.45 179.07 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "92000947-bcc3-4b24-8379-ac97db4b9784")
	)
	(label "Supply"
		(at 173.99 152.4 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "97ef7d25-bb4d-43ce-8596-dbd67ee245c0")
	)
	(label "Vdump"
		(at 279.4 191.77 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "9b9f14db-6859-4d4d-96fe-71fb1d5c0f97")
	)
	(label "Vref"
		(at 228.6 31.75 180)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify right bottom)
		)
		(uuid "aa2be03c-2736-4ee9-ac72-67640f0c21ac")
	)
	(label "Vin"
		(at 97.79 43.18 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "ad49b1fa-a354-4171-8959-efb8eb32ad93")
	)
	(label "24V"
		(at 81.28 121.92 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "aefaa400-40bb-4be1-a0f1-c751a5c076a4")
	)
	(label "Vin"
		(at 30.48 110.49 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "ba119d78-800e-43c7-ae23-d854548961e3")
	)
	(label "Vin"
		(at 237.49 162.56 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "bb929095-c225-4167-92c6-485e564f1165")
	)
	(label "Vdump"
		(at 279.4 201.93 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "c1b8ddf3-da34-446e-a2d4-fcba915d015a")
	)
	(label "GND"
		(at 189.23 33.02 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "c781a0e0-b5d3-4517-b548-d6342cf023cc")
	)
	(label "Vin"
		(at 38.1 41.91 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "d6d002a2-0461-4c49-ae0d-8a7dc623e48b")
	)
	(label "Supply"
		(at 245.11 55.88 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "d86d77ce-100f-41c1-8e4f-9792c618ddd4")
	)
	(label "Supply"
		(at 119.38 59.69 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "dba673be-33eb-4c05-991f-f42891428644")
	)
	(label "Sensor_VDD"
		(at 31.75 73.66 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "dc568acf-6f72-450b-97ea-eec7cee3cd5c")
	)
	(label "Vref"
		(at 171.45 50.8 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "dcc463e0-5c72-47f1-85d3-9c57b77f717e")
	)
	(label "Vout_sensor"
		(at 205.74 72.39 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "dd77ca39-068d-4569-999a-f9946fad7577")
	)
	(label "Vdump"
		(at 39.37 250.19 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "e6845911-2aee-4d87-816a-439094c96c19")
	)
	(label "Vin"
		(at 33.02 66.04 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "f0957a19-ea7c-47ae-9a76-5de7151a4e68")
	)
	(label "Supply"
		(at 245.11 62.23 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "f444d2d5-3030-4ec3-b883-e8249ce7740c")
	)
	(label "48V"
		(at 78.74 119.38 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "f76c6dbe-8498-44e4-99a5-8e329e1ea919")
	)
	(label "Vref"
		(at 231.14 67.31 0)
		(effects
			(font
				(size 1.524 1.524)
			)
			(justify left bottom)
		)
		(uuid "fa05239e-fb6b-41e9-a5a2-f5cf2a25494b")
	)
	(symbol
		(lib_id "Regulator_Controller:SG3525")
		(at 118.11 181.61 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ace3170")
		(property "Reference" "IC1"
			(at 119.38 180.34 0)
			(effects
				(font
					(size 1.524 1.524)
				)
			)
		)
		(property "Value" "SG3525"
			(at 477.52 353.06 0)
			(effects
				(font
					(size 1.524 1.524)
				)
			)
		)
		(property "Footprint" "Housings_DIP:DIP-16_W7.62mm"
			(at 477.52 353.06 0)
			(effects
				(font
					(size 1.524 1.524)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 477.52 353.06 0)
			(effects
				(font
					(size 1.524 1.524)
				)
			)
		)
		(property "Description" ""
			(at 118.11 181.61 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "5"
			(uuid "e6f90cbd-f5fc-4a3d-a5f0-f749a393d871")
		)
		(pin "6"
			(uuid "2096a3d5-31d4-49af-a9bb-6c9d9356519b")
		)
		(pin "1"
			(uuid "97b999bb-7dc4-493d-97a5-36f11c7a499a")
		)
		(pin "10"
			(uuid "1308a912-612a-43df-a738-7c93caa093ff")
		)
		(pin "11"
			(uuid "2d2c7361-b516-490f-82df-e7eddde17490")
		)
		(pin "12"
			(uuid "9bca4e58-ae14-4f48-acd6-6898119c7347")
		)
		(pin "13"
			(uuid "dc30eeba-204f-4dc9-8191-b467eaaff3df")
		)
		(pin "14"
			(uuid "2e1b69c4-2cb8-40a6-8b8c-dfbfbf3142b6")
		)
		(pin "15"
			(uuid "4bea7b03-91ee-4773-8574-1d051b8d6c33")
		)
		(pin "16"
			(uuid "5e78774d-7d16-4751-bc35-bb633a02faa2")
		)
		(pin "2"
			(uuid "b8ab5484-7ce5-461a-b977-9d1eb088333e")
		)
		(pin "9"
			(uuid "b257b482-41f2-45a1-87a8-0d752561951c")
		)
		(pin "8"
			(uuid "abeb2762-2e69-42b7-a1d3-71e7c10c2d37")
		)
		(pin "4"
			(uuid "1459dccb-4e29-441c-8973-1e96f315f0de")
		)
		(pin "7"
			(uuid "93af3ed3-be37-46f8-86d0-5ee7f3c0936f")
		)
		(pin "3"
			(uuid "cc6874ea-9d71-410c-a91a-64d02f6a834b")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "IC1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:PWR_FLAG-power")
		(at 189.23 33.02 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005acf5a5d")
		(property "Reference" "#FLG01"
			(at 189.23 31.115 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "PWR_FLAG"
			(at 189.23 29.21 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Footprint" ""
			(at 189.23 33.02 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 189.23 33.02 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 189.23 33.02 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "aa0eeca6-c45f-4340-923b-5e3cf30a5767")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#FLG01")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:PWR_FLAG-power")
		(at 35.56 34.29 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005acf5af4")
		(property "Reference" "#FLG02"
			(at 35.56 32.385 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "PWR_FLAG"
			(at 35.56 30.48 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 35.56 34.29 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 35.56 34.29 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 35.56 34.29 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "d29ab0be-22e0-4587-9bb5-5d93d088ea69")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#FLG02")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 160.02 45.72 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005acf5be1")
		(property "Reference" "R18"
			(at 160.02 47.752 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "2.2k"
			(at 160.02 45.72 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 160.02 43.942 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 160.02 45.72 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 160.02 45.72 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "8a684d46-78a4-48b3-b778-66aa4439ceb9")
		)
		(pin "2"
			(uuid "4de04cec-4470-45d8-9861-b8c3b7b117eb")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R18")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 177.8 35.56 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005acf5cc4")
		(property "Reference" "R19"
			(at 177.8 37.592 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "2.2k"
			(at 177.8 35.56 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 177.8 33.782 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 177.8 35.56 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 177.8 35.56 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "6f97cab1-dd85-455c-b641-d5fca4dda38a")
		)
		(pin "2"
			(uuid "9a715e91-aaee-4def-8c02-8ff2d5918870")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R19")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 156.21 45.72 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005acf5e12")
		(property "Reference" "#PWR03"
			(at 149.86 45.72 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 152.4 45.72 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 156.21 45.72 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 156.21 45.72 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 156.21 45.72 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "289bf4f6-d1cc-427a-b832-4eb1f29ff4ff")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR03")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:D_Zener")
		(at 90.17 59.69 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad074c8")
		(property "Reference" "D1"
			(at 92.71 59.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "1N5353B"
			(at 87.63 59.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Diodes_THT:D_DO-15_P12.70mm_Horizontal"
			(at 90.17 59.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 90.17 59.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 90.17 59.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "d402f61f-3d97-4f47-a460-12ddf69a443f")
		)
		(pin "2"
			(uuid "a9d8745e-d42c-4391-8dcf-006ec9531480")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "D1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 90.17 49.53 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad076be")
		(property "Reference" "R9"
			(at 92.202 49.53 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "1.5k"
			(at 90.17 49.53 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistor_THT:R_Axial_Power_L38.0mm_W9.0mm_P45.72mm"
			(at 88.392 49.53 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 90.17 49.53 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 90.17 49.53 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "98de2381-97ab-4bce-b11c-a7b1390031af")
		)
		(pin "1"
			(uuid "8d5beb37-baa0-4446-8ab1-75f144a62329")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R9")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:Q_NPN_BCE")
		(at 100.33 54.61 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad07701")
		(property "Reference" "Q1"
			(at 105.41 53.34 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "FJA43100TU"
			(at 105.41 55.88 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "TO_SOT_Packages_THT:TO-3PB__Vertical"
			(at 105.41 52.07 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 100.33 54.61 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 100.33 54.61 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "97b270ef-7635-4366-837c-993160e8e372")
		)
		(pin "1"
			(uuid "51341105-b895-4e6c-b4de-9f8debf1ecee")
		)
		(pin "3"
			(uuid "8946158e-cff8-4384-a096-9b0837a9fbab")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "Q1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:C")
		(at 109.22 63.5 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad07769")
		(property "Reference" "C1"
			(at 109.855 60.96 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "100n"
			(at 109.855 66.04 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm"
			(at 110.1852 67.31 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 109.22 63.5 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 109.22 63.5 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "94d5f92f-c346-4a6c-8437-980ca4ccc736")
		)
		(pin "2"
			(uuid "cda9ddc8-fb5e-4471-b8aa-a6eb978eed02")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "C1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 100.33 67.31 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad07877")
		(property "Reference" "#PWR04"
			(at 100.33 73.66 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 100.33 71.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 100.33 67.31 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 100.33 67.31 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 100.33 67.31 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "f2011355-419e-46a5-9151-809a7b132333")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR04")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 52.07 116.84 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad07cd3")
		(property "Reference" "R5"
			(at 54.102 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "20k"
			(at 52.07 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 50.292 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 52.07 116.84 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 52.07 116.84 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "71702e28-64a6-46e4-92b5-71ccac592321")
		)
		(pin "2"
			(uuid "6f23ef34-a7c6-4938-82b6-925b4d783796")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R5")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 52.07 128.27 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad07d52")
		(property "Reference" "R6"
			(at 54.102 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "11k"
			(at 52.07 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 50.292 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 52.07 128.27 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 52.07 128.27 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "ccb1f0e0-1164-4bca-8853-63a07251266b")
		)
		(pin "2"
			(uuid "e151afa2-cfd0-445d-aecc-1a0139d1b0d7")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R6")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 59.69 128.27 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad07dfb")
		(property "Reference" "R11"
			(at 61.722 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "4.3k"
			(at 59.69 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 57.912 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 59.69 128.27 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 59.69 128.27 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "aee9e1ff-54b3-44da-81cb-30b285f471a8")
		)
		(pin "1"
			(uuid "13544e84-7e0a-46a0-82db-4a3a34766cee")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R11")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 67.31 116.84 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad07e58")
		(property "Reference" "R13"
			(at 69.342 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "105k"
			(at 67.31 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 65.532 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 67.31 116.84 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 67.31 116.84 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "d2bee4bb-74cc-458e-aaf3-05c4237d6f8b")
		)
		(pin "1"
			(uuid "1cee3514-5306-4ad0-bea8-ef70856c5206")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R13")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 67.31 128.27 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad07eaa")
		(property "Reference" "R14"
			(at 69.342 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10.2k"
			(at 67.31 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 65.532 128.27 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 67.31 128.27 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 67.31 128.27 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "94ee6194-3684-4b06-96ca-6c703df2891f")
		)
		(pin "2"
			(uuid "b96e9e56-9bf4-486a-98c8-80f54ec1df28")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R14")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 59.69 137.16 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad08023")
		(property "Reference" "#PWR05"
			(at 59.69 143.51 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 59.69 140.97 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 59.69 137.16 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 59.69 137.16 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 59.69 137.16 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "147d7b70-5c2a-42ce-a744-1f5c7f27cae8")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR05")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:SW_SP3T")
		(at 95.25 121.92 180)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad086e0")
		(property "Reference" "SW1"
			(at 95.25 127 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "JS203011CQN"
			(at 95.25 116.84 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Switchs_perso:SW_SP3T"
			(at 111.125 126.365 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 111.125 126.365 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 95.25 121.92 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "4"
			(uuid "5ed42aac-8987-41f2-9cbf-3ef1a0853e76")
		)
		(pin "1"
			(uuid "518a4fcb-a590-4ade-9960-34b796a739bb")
		)
		(pin "2"
			(uuid "0caf1f41-f759-4cfe-8444-ed1c252c8fe0")
		)
		(pin "3"
			(uuid "f1745a01-3fcd-4d5c-a691-bbb3c7fe747b")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "SW1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:LM358")
		(at 191.77 48.26 0)
		(mirror x)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad08d23")
		(property "Reference" "U1"
			(at 191.77 53.34 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "LM358N"
			(at 191.77 43.18 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Housings_DIP:DIP-8_W7.62mm"
			(at 191.77 48.26 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 191.77 48.26 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 191.77 48.26 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "4"
			(uuid "adc13c2f-91a2-4929-b54a-716793ef3424")
		)
		(pin "8"
			(uuid "14e9aaeb-9f4f-4fd9-9d6a-ab1b85025b44")
		)
		(pin "1"
			(uuid "695ed2fb-1667-463a-b555-45f475ab75cd")
		)
		(pin "2"
			(uuid "d10bf432-5eba-4d6e-b176-27d61fe336d4")
		)
		(pin "3"
			(uuid "f82d3942-7a62-413f-bba2-1ba6d2ec0d36")
		)
		(pin "5"
			(uuid "3607b8ba-4454-4b9d-bd2f-6e28e77b1e37")
		)
		(pin "6"
			(uuid "b1e766a0-be47-467a-ba1d-da78c60bf051")
		)
		(pin "7"
			(uuid "e96099bd-338a-4e38-b2e4-9404a9e37f7a")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "U1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 223.52 40.64 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad09dfb")
		(property "Reference" "R20"
			(at 225.552 40.64 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10k"
			(at 223.52 40.64 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 221.742 40.64 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 223.52 40.64 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 223.52 40.64 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "c63be6d6-fc6c-45be-9eb4-ab315cbb81e0")
		)
		(pin "2"
			(uuid "2e9f9a7c-e185-42d2-b898-b347335befee")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R20")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 223.52 50.8 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad09e57")
		(property "Reference" "R21"
			(at 225.552 50.8 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10k"
			(at 223.52 50.8 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 221.742 50.8 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 223.52 50.8 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 223.52 50.8 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "a88c921f-46c1-442e-910a-d538d52d8a26")
		)
		(pin "2"
			(uuid "e19442ae-96eb-4ef5-a300-d4d45aae570a")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R21")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 223.52 57.15 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad09eb5")
		(property "Reference" "#PWR06"
			(at 223.52 63.5 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 223.52 60.96 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 223.52 57.15 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 223.52 57.15 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 223.52 57.15 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "d947fc10-2963-4d91-8678-7daa48df7407")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR06")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:LM358")
		(at 247.65 41.91 0)
		(mirror x)
		(unit 2)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad09f11")
		(property "Reference" "U1"
			(at 247.65 46.99 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "LM358N"
			(at 250.19 44.45 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Housings_DIP:DIP-8_W7.62mm"
			(at 247.65 41.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 247.65 41.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 247.65 41.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "afec9e03-5b13-45a3-89a4-5d13028145d3")
		)
		(pin "7"
			(uuid "2b320e42-623c-4d10-b3a9-be61e1373ad2")
		)
		(pin "8"
			(uuid "025038bb-bffc-4dee-b9e2-60a1d93e86f2")
		)
		(pin "4"
			(uuid "9ecb148d-3811-4870-8cc9-b9f2d750ba87")
		)
		(pin "2"
			(uuid "8f3d94f9-c8a3-4487-84a8-381832c302c2")
		)
		(pin "6"
			(uuid "b98b3db0-3216-48b2-ad12-4c62383a95e7")
		)
		(pin "3"
			(uuid "6b57f3e1-755d-4a7b-9a68-6f7f1a6a8206")
		)
		(pin "5"
			(uuid "f0cea286-a993-45f1-95a2-eb033c975fe5")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "U1")
					(unit 2)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 245.11 31.75 0)
		(mirror x)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0a2dc")
		(property "Reference" "#PWR07"
			(at 245.11 25.4 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 245.11 27.94 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 245.11 31.75 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 245.11 31.75 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 245.11 31.75 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "200edd98-4936-4079-b706-7a6892129716")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR07")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 236.22 39.37 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0a421")
		(property "Reference" "R23"
			(at 236.22 41.402 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10k"
			(at 236.22 39.37 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 236.22 37.592 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 236.22 39.37 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 236.22 39.37 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "c91b5365-cc82-4c04-94b8-abe6d36ca63a")
		)
		(pin "1"
			(uuid "e649aea4-e78d-4035-aaed-5560ee950b51")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R23")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 232.41 39.37 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0ac8f")
		(property "Reference" "#PWR08"
			(at 226.06 39.37 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 228.6 39.37 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 232.41 39.37 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 232.41 39.37 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 232.41 39.37 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "619d7cfd-3e44-4c5b-8108-1f74ed94a116")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR08")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 76.2 186.69 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0b620")
		(property "Reference" "R15"
			(at 76.2 184.404 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "47k"
			(at 76.2 186.69 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 76.2 184.912 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 76.2 186.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 76.2 186.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "7e718d37-00ac-4a71-a40a-2927ce35f1b6")
		)
		(pin "1"
			(uuid "7a17bb61-a2f4-4848-a2df-c4057b881085")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R15")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 90.17 191.77 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0b6ae")
		(property "Reference" "R17"
			(at 90.17 193.802 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "22"
			(at 90.17 191.77 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 90.17 189.992 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 90.17 191.77 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 90.17 191.77 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "97957891-be11-4787-9c20-fc6bc1d54366")
		)
		(pin "2"
			(uuid "7271ded6-5432-43c8-a21b-a2df1b652c26")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R17")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:C")
		(at 68.58 200.66 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0b731")
		(property "Reference" "CT1"
			(at 69.215 198.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "100n"
			(at 69.215 203.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm"
			(at 69.5452 204.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 68.58 200.66 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 68.58 200.66 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "2fce5fc8-643b-47a9-9b80-684f2a001f67")
		)
		(pin "1"
			(uuid "cf978f79-2559-4b26-8894-30a5beca917b")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "CT1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:C")
		(at 81.28 204.47 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0b7b3")
		(property "Reference" "CSS1"
			(at 81.915 201.93 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "10n"
			(at 81.915 207.01 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm"
			(at 82.2452 208.28 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 81.28 204.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 81.28 204.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "8f094ff0-8215-4a5e-81e3-fac4e5b1ebc1")
		)
		(pin "1"
			(uuid "6942f451-eabf-47a2-ac4c-a698920ccbeb")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "CSS1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 73.66 208.28 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0bb9e")
		(property "Reference" "#PWR09"
			(at 73.66 214.63 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 73.66 212.09 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 73.66 208.28 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 73.66 208.28 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 73.66 208.28 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "cbe475f6-e290-4807-b0a3-ac1813234cec")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR09")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:C")
		(at 92.71 173.99 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0c260")
		(property "Reference" "C2"
			(at 95.25 174.625 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "2.2u"
			(at 90.17 174.625 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm"
			(at 88.9 174.9552 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 92.71 173.99 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 92.71 173.99 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "b3a2a330-da99-4623-b71f-21b4875b4153")
		)
		(pin "2"
			(uuid "4fe52c03-9ba7-443e-8679-777825616b93")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "C2")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 82.55 173.99 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0c2df")
		(property "Reference" "R16"
			(at 82.55 176.022 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "36k"
			(at 82.55 173.99 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 82.55 172.212 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 82.55 173.99 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 82.55 173.99 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "1b706dbc-a6e6-414d-8529-70c562b1ca02")
		)
		(pin "2"
			(uuid "df07e2a1-9624-468d-8e69-07cda48782e4")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R16")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 66.04 171.45 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0c602")
		(property "Reference" "R12"
			(at 66.04 173.482 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "36k"
			(at 66.04 171.45 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 66.04 169.672 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 66.04 171.45 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 66.04 171.45 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "640e513a-0e0a-4622-aea4-5bf41f5bbaaa")
		)
		(pin "1"
			(uuid "5ab4f462-0eca-4e1d-9fb8-4092ae473354")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R12")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:LM358")
		(at 176.53 140.97 0)
		(mirror x)
		(unit 2)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0d786")
		(property "Reference" "U3"
			(at 176.53 146.05 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "LM358N"
			(at 176.53 135.89 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Housings_DIP:DIP-8_W7.62mm"
			(at 176.53 140.97 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 176.53 140.97 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 176.53 140.97 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "4"
			(uuid "cccead2e-5a3d-4adc-817a-fb9f3f388178")
		)
		(pin "8"
			(uuid "12ab7579-24b5-46e0-af2f-df4663d58d54")
		)
		(pin "1"
			(uuid "9baf1277-f997-4880-ab04-c74177e5922d")
		)
		(pin "2"
			(uuid "e2737676-c89c-44d0-babb-904de0dd2030")
		)
		(pin "3"
			(uuid "67813d6d-3dfc-4cd6-a1a1-77b23cb8698e")
		)
		(pin "5"
			(uuid "a3875d1f-e6ed-48ce-96cb-7ddea5e1d23c")
		)
		(pin "7"
			(uuid "a9bc7e52-4131-4549-9267-2e2d40bafa10")
		)
		(pin "6"
			(uuid "0730136a-3cf0-4d16-8aa8-8d4a03b27d93")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "U3")
					(unit 2)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 173.99 133.35 0)
		(mirror x)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0daa8")
		(property "Reference" "#PWR010"
			(at 173.99 127 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 173.99 129.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 173.99 133.35 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 173.99 133.35 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 173.99 133.35 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "ebff3f93-9f6d-4d30-be14-be711625fad1")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR010")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:LM358")
		(at 114.3 119.38 0)
		(mirror x)
		(unit 2)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0eb4c")
		(property "Reference" "U2"
			(at 114.3 124.46 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "LM358N"
			(at 114.3 114.3 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Housings_DIP:DIP-8_W7.62mm"
			(at 114.3 119.38 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 114.3 119.38 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 114.3 119.38 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "1323d92f-2bc5-4531-b537-8094d59f0561")
		)
		(pin "1"
			(uuid "4c4f6fd2-a8b1-4fae-a68c-ba739323f21d")
		)
		(pin "7"
			(uuid "c20684bd-f81e-497b-b24c-cc2bffba4a81")
		)
		(pin "5"
			(uuid "ef448466-383d-4854-baff-ef2b56370b5e")
		)
		(pin "8"
			(uuid "90f9fe66-e5dc-403f-9e3b-3caa16e3da49")
		)
		(pin "6"
			(uuid "9fdfa65a-97a6-4d45-b3fb-5fc6fa3a4b72")
		)
		(pin "4"
			(uuid "3da2f62e-e877-4d62-a6e1-b2ff71e11085")
		)
		(pin "3"
			(uuid "8eb7ab77-cebb-4705-a97b-705849c70b93")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "U2")
					(unit 2)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 111.76 105.41 0)
		(mirror x)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad0fd79")
		(property "Reference" "#PWR011"
			(at 111.76 99.06 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 111.76 101.6 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 111.76 105.41 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 111.76 105.41 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 111.76 105.41 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "8b888373-fb9c-4fd4-a861-a43f7b0fad0a")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR011")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:LM358")
		(at 247.65 69.85 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad104d2")
		(property "Reference" "U2"
			(at 247.65 64.77 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "LM358N"
			(at 247.65 74.93 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Housings_DIP:DIP-8_W7.62mm"
			(at 247.65 69.85 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 247.65 69.85 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 247.65 69.85 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "7"
			(uuid "0eabab01-d850-4abe-bb43-540ae11331d7")
		)
		(pin "4"
			(uuid "c3726e25-474e-4002-988c-bbf04d2b94df")
		)
		(pin "8"
			(uuid "6d25a008-c3f2-4a2d-88a6-4da35a505e82")
		)
		(pin "1"
			(uuid "fbabf5ca-baf5-4b37-9532-f98d1fa12d5f")
		)
		(pin "6"
			(uuid "7b461771-06c8-4271-97f4-442e200ffcb5")
		)
		(pin "2"
			(uuid "8dfaff6c-382d-4355-a41b-61fb648e940c")
		)
		(pin "3"
			(uuid "b7f66ce9-ebfc-46a1-ba42-bf6a3b1ec48a")
		)
		(pin "5"
			(uuid "1c1e34fb-c6c7-45a2-8d03-4349306574a4")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "U2")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 232.41 72.39 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad107dc")
		(property "Reference" "R22"
			(at 232.41 74.422 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "13k"
			(at 232.41 72.39 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 232.41 70.612 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 232.41 72.39 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 232.41 72.39 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "22901f73-466c-47f7-9a06-95ff828fa5ab")
		)
		(pin "2"
			(uuid "c06db932-c94c-42c9-a70c-1d1dd982e678")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R22")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 245.11 83.82 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad1088a")
		(property "Reference" "R25"
			(at 245.11 85.852 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "11k"
			(at 245.11 83.82 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 245.11 82.042 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 245.11 83.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 245.11 83.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "b860c449-8fc1-4d04-abaa-565150e3b15a")
		)
		(pin "2"
			(uuid "4f8a252f-5c35-4d99-b4ff-3841124a1784")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R25")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 245.11 77.47 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad1151c")
		(property "Reference" "#PWR012"
			(at 245.11 83.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 245.11 81.28 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 245.11 77.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 245.11 77.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 245.11 77.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "cef5eb2e-d5cb-41ad-8fd7-6549c95eeccf")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR012")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:LM358")
		(at 46.99 171.45 0)
		(mirror x)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad11a82")
		(property "Reference" "U3"
			(at 46.99 176.53 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "LM358N"
			(at 46.99 166.37 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Housings_DIP:DIP-8_W7.62mm"
			(at 46.99 171.45 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 46.99 171.45 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 46.99 171.45 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "4"
			(uuid "71311607-ac7d-4f76-9216-1aea18dcd9e6")
		)
		(pin "3"
			(uuid "2d7467f9-cb06-42a5-a41f-72e1315cbcfa")
		)
		(pin "2"
			(uuid "65f0e4e6-e9d0-4a9d-9d79-99dbd3afeb48")
		)
		(pin "6"
			(uuid "b1a9fb13-b13a-4aa3-92e1-ecd1621a6125")
		)
		(pin "7"
			(uuid "089c4f26-830d-463e-8765-09b7f2fb54ca")
		)
		(pin "5"
			(uuid "aae62181-642a-4738-bb54-7d19fb009bea")
		)
		(pin "8"
			(uuid "6def21a7-107a-4387-b828-8071bc07f502")
		)
		(pin "1"
			(uuid "64de9590-7104-4cdb-b038-9d904bc467f7")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "U3")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 27.94 168.91 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad11e57")
		(property "Reference" "R1"
			(at 27.94 170.942 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10k"
			(at 27.94 168.91 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 27.94 167.132 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 27.94 168.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 27.94 168.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "d9f64657-d63a-4a9f-b64e-e726fe0e29b2")
		)
		(pin "1"
			(uuid "1b580518-c140-48e4-89ca-78412fc5b27b")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 27.94 173.99 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad11eea")
		(property "Reference" "R2"
			(at 27.94 176.022 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10k"
			(at 27.94 173.99 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 27.94 172.212 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 27.94 173.99 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 27.94 173.99 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "325aadb9-b800-4adf-9111-f6bb1b1f3efd")
		)
		(pin "2"
			(uuid "ec0a2d7a-0723-4a9e-ba67-54a6d782d2aa")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R2")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 35.56 179.07 180)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad11f72")
		(property "Reference" "R3"
			(at 33.528 179.07 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10k"
			(at 35.56 179.07 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 37.338 179.07 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 35.56 179.07 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 35.56 179.07 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "ddc8f2d9-b038-4b00-9ff2-c76ef0fa4e20")
		)
		(pin "2"
			(uuid "66e4c60f-af4b-4a0d-9931-5409bc26171c")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R3")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 46.99 154.94 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad12009")
		(property "Reference" "R4"
			(at 46.99 156.972 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10k"
			(at 46.99 154.94 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 46.99 153.162 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 46.99 154.94 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 46.99 154.94 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "76d5a0f4-9c09-4fcd-a975-a744359dc378")
		)
		(pin "2"
			(uuid "a0857bf9-a272-4b98-a9f4-cde9d287e6a8")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R4")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 44.45 163.83 180)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad12869")
		(property "Reference" "#PWR013"
			(at 44.45 157.48 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 44.45 160.02 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 44.45 163.83 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 44.45 163.83 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 44.45 163.83 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "3b8f66dc-0b2e-4000-9933-215beddb1aba")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR013")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:IRLZ44N")
		(at 234.95 182.88 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad46b8f")
		(property "Reference" "Q4"
			(at 241.3 179.07 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "IRFB4321"
			(at 241.3 181.61 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "TO_SOT_Packages_THT:TO-220-3_Vertical"
			(at 241.3 184.785 0)
			(effects
				(font
					(size 1.27 1.27)
					(italic yes)
				)
				(justify left)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 234.95 182.88 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 234.95 182.88 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "9c1aaeb2-b48d-4d9a-8022-1f333a4ffe31")
		)
		(pin "2"
			(uuid "d21b3e7c-0c47-4499-b2b6-a19798cf5926")
		)
		(pin "3"
			(uuid "8d68b8a1-520d-49fc-9662-fc75cf468fe3")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "Q4")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:IRLZ44N")
		(at 247.65 198.12 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad46f3e")
		(property "Reference" "Q3"
			(at 254 196.215 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "IRFB4321"
			(at 254 198.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "TO_SOT_Packages_THT:TO-220-3_Vertical"
			(at 254 200.025 0)
			(effects
				(font
					(size 1.27 1.27)
					(italic yes)
				)
				(justify left)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 247.65 198.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 247.65 198.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "7dded8e2-b57d-477e-878e-dd77f50423ca")
		)
		(pin "2"
			(uuid "27b0fb2e-e3b7-4693-a94c-340fd37a9f46")
		)
		(pin "3"
			(uuid "f0b9bea9-4cc1-4ff2-922b-6ff9dfb995ec")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "Q3")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:IRLZ44N")
		(at 261.62 210.82 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad47004")
		(property "Reference" "Q2"
			(at 267.97 208.915 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "IRFB4321"
			(at 267.97 210.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "TO_SOT_Packages_THT:TO-220-3_Vertical"
			(at 267.97 212.725 0)
			(effects
				(font
					(size 1.27 1.27)
					(italic yes)
				)
				(justify left)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 261.62 210.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 261.62 210.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Package" "TO220"
			(at 261.62 210.82 0)
			(effects
				(font
					(size 1.524 1.524)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "61c123f7-03bf-4ef9-9922-55ad3c1b33a0")
		)
		(pin "2"
			(uuid "ae58612a-21b5-42c7-87b4-90db9c1f418e")
		)
		(pin "3"
			(uuid "1bfd8b8b-4ab7-49fa-878a-3b41d4df47e1")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "Q2")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 219.71 182.88 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad47166")
		(property "Reference" "R26"
			(at 219.71 184.912 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "22"
			(at 219.71 182.88 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_perso:R_Axial_DIN0207_L40mm_D2.5mm_P15.24mm_Horizontal"
			(at 219.71 181.102 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 219.71 182.88 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 219.71 182.88 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "48187934-89f7-4577-8096-af337e721258")
		)
		(pin "2"
			(uuid "f3d007de-a8d3-4831-9048-0f5887dd89fd")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R26")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 219.71 198.12 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad472d6")
		(property "Reference" "R27"
			(at 219.71 200.152 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "22"
			(at 219.71 198.12 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_perso:R_Axial_DIN0207_L40mm_D2.5mm_P15.24mm_Horizontal"
			(at 219.71 196.342 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 219.71 198.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 219.71 198.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "c00fd275-2ab9-4f8b-8ae1-2112bec4d30a")
		)
		(pin "2"
			(uuid "19572630-1e4c-4054-ae7f-f482f3841873")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R27")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 219.71 210.82 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad473ae")
		(property "Reference" "R28"
			(at 219.71 212.852 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "22"
			(at 219.71 210.82 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_perso:R_Axial_DIN0207_L40mm_D2.5mm_P15.24mm_Horizontal"
			(at 219.71 209.042 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 219.71 210.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 219.71 210.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "7c37cf54-faef-4b70-95dc-3b15ccee467c")
		)
		(pin "2"
			(uuid "6802892a-bb3b-4615-8fef-5754e84189e4")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R28")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 204.47 233.68 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad47d18")
		(property "Reference" "R24"
			(at 206.502 233.68 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "1.5k"
			(at 204.47 233.68 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 202.692 233.68 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 204.47 233.68 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 204.47 233.68 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "3ad14c8d-f075-42d8-b2ba-b16eb6ead939")
		)
		(pin "2"
			(uuid "36ccb8ea-ac6b-487e-a840-1cbc5c091559")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R24")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 204.47 240.03 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad48005")
		(property "Reference" "#PWR014"
			(at 204.47 246.38 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 204.47 243.84 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 204.47 240.03 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 204.47 240.03 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 204.47 240.03 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "fdffa369-0813-4bc4-97a9-c42ce93b1e84")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR014")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 264.16 215.9 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad4a3e4")
		(property "Reference" "#PWR015"
			(at 264.16 222.25 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 264.16 219.71 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 264.16 215.9 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 264.16 215.9 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 264.16 215.9 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "ae1bf1bd-8fc7-4204-81eb-9c0448f5eac9")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR015")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 250.19 203.2 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad4a486")
		(property "Reference" "#PWR016"
			(at 250.19 209.55 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 250.19 207.01 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 250.19 203.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 250.19 203.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 250.19 203.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "ec290c50-da30-49a5-b556-3e90ec580590")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR016")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 237.49 187.96 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad4a521")
		(property "Reference" "#PWR017"
			(at 237.49 194.31 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 237.49 191.77 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 237.49 187.96 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 237.49 187.96 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 237.49 187.96 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "4c1ac4ea-555f-417a-944c-939e37a86f6b")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR017")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 118.11 199.39 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad4bc90")
		(property "Reference" "#PWR018"
			(at 118.11 205.74 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 118.11 203.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 118.11 199.39 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 118.11 199.39 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 118.11 199.39 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "167e8a17-fb2d-461e-8259-60e6a53a1ca6")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR018")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 59.69 116.84 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad76da2")
		(property "Reference" "R10"
			(at 61.722 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "20k"
			(at 59.69 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 57.912 116.84 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 59.69 116.84 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 59.69 116.84 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "00acc3de-bd85-4dfb-903b-b9623769fbe1")
		)
		(pin "2"
			(uuid "1307c6ba-59d6-4c0b-9fc2-9d8bbccd9576")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R10")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:POT")
		(at 251.46 33.02 90)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ad78034")
		(property "Reference" "RV1"
			(at 251.46 37.465 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "10k"
			(at 251.46 35.56 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Potentiometer_THT:Potentiometer_Bourns_3386C_Horizontal"
			(at 251.46 33.02 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 251.46 33.02 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 251.46 33.02 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "b643aa32-00cc-4a77-b4a4-45d6308406f5")
		)
		(pin "2"
			(uuid "a2caa2cd-8ab7-4a83-83f7-a6435521cebd")
		)
		(pin "3"
			(uuid "f87e0a88-189b-4dda-ad62-738fc2f1c20f")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "RV1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 35.56 49.53 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ade0644")
		(property "Reference" "#PWR019"
			(at 35.56 55.88 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 35.56 53.34 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 35.56 49.53 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 35.56 49.53 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 35.56 49.53 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "65d669ed-9ee4-4424-905e-b5da3cc7b877")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR019")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:Conn_01x03")
		(at 52.07 76.2 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ade5491")
		(property "Reference" "J3"
			(at 52.07 71.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "Conn_01x03"
			(at 52.07 81.28 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x03_P3.50mm_Horizontal"
			(at 52.07 76.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 52.07 76.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 52.07 76.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "cc8fb531-e65b-49a9-86ed-63678ce142dc")
		)
		(pin "2"
			(uuid "06df4df5-ea65-4be0-b091-965ebdae558d")
		)
		(pin "3"
			(uuid "dce92e49-7af2-4808-aefe-39d1d91ea5a0")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "J3")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 99.06 180.34 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ade6f12")
		(property "Reference" "#PWR020"
			(at 99.06 186.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 95.504 180.594 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 99.06 180.34 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 99.06 180.34 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 99.06 180.34 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "a1c1d117-0b10-4b16-aa07-edb149455b57")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR020")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:C")
		(at 193.04 138.43 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae10082")
		(property "Reference" "C4"
			(at 193.675 135.89 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "100n"
			(at 193.675 140.97 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm"
			(at 194.0052 142.24 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 193.04 138.43 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 193.04 138.43 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "e5ec5214-731a-411c-8375-27d70adeb102")
		)
		(pin "2"
			(uuid "83fb5f25-29c4-46c0-86db-7443298c85dc")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "C4")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 193.04 142.24 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae10523")
		(property "Reference" "#PWR021"
			(at 193.04 148.59 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 193.04 146.05 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 193.04 142.24 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 193.04 142.24 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 193.04 142.24 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "e7d347da-68bf-4cbd-904e-47130eb36779")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR021")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 44.45 238.76 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae148eb")
		(property "Reference" "#PWR022"
			(at 38.1 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 40.64 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 44.45 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 44.45 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 44.45 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "7d15eac0-fb1a-471b-a0d7-699e7359e019")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR022")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 52.07 238.76 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae14a31")
		(property "Reference" "R7"
			(at 52.07 240.792 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "1k"
			(at 52.07 238.76 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal"
			(at 52.07 236.982 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 52.07 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 52.07 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "af62eecb-623f-488a-a760-a2ab5191aec5")
		)
		(pin "2"
			(uuid "358add7a-6998-4c1f-b9ec-68f882cd9e73")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R7")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:LED")
		(at 64.77 238.76 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae14b2a")
		(property "Reference" "D2"
			(at 64.77 236.22 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "LED L-53GD"
			(at 66.04 242.57 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "LED_THT:LED_D5.0mm"
			(at 64.77 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 64.77 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 64.77 238.76 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "b9547c88-85ac-4ca0-9be7-615cb2bbe614")
		)
		(pin "1"
			(uuid "0d9bb9d5-32ad-427a-b19e-8b00c3958c8c")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "D2")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:LED")
		(at 64.77 250.19 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae14be3")
		(property "Reference" "D3"
			(at 64.77 247.65 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "LED L-53HD"
			(at 66.04 254 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "LED_THT:LED_D5.0mm"
			(at 64.77 250.19 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 64.77 250.19 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 64.77 250.19 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "670edb3c-738c-4bd7-9f3e-1656917b2a1b")
		)
		(pin "1"
			(uuid "735556fb-d05d-48fc-a07a-1c96c292d52a")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "D3")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 52.07 250.19 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae14d08")
		(property "Reference" "R8"
			(at 52.07 252.222 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "4.7k"
			(at 52.07 250.19 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal"
			(at 52.07 248.412 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 52.07 250.19 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 52.07 250.19 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "cff2eec4-c4be-45f9-9f1e-7773b4436a4a")
		)
		(pin "1"
			(uuid "e2498fec-7c2c-417d-b349-a0ab4ce7cbd7")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R8")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:D")
		(at 237.49 168.91 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae18bed")
		(property "Reference" "D6"
			(at 240.03 168.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "SR5100"
			(at 234.95 168.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Diode_THT:D_DO-27_P15.24mm_Horizontal"
			(at 237.49 168.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 237.49 168.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 237.49 168.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "d3b56d85-b71f-4093-97c4-32d490091e7e")
		)
		(pin "2"
			(uuid "689bee10-cef3-4fc8-9a39-d723eeebe3a4")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "D6")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:PWR_FLAG-power")
		(at 118.11 54.61 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ae18fb6")
		(property "Reference" "#FLG023"
			(at 118.11 52.705 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "PWR_FLAG"
			(at 118.11 50.8 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 118.11 54.61 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 118.11 54.61 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 118.11 54.61 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "daf6bc79-5be5-4a3e-986d-7f772a6a1a25")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#FLG023")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:IRLZ44N")
		(at 273.05 223.52 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005afdb25b")
		(property "Reference" "Q5"
			(at 279.4 221.615 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "IRFB4321"
			(at 279.4 223.52 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "TO_SOT_Packages_THT:TO-220-3_Vertical"
			(at 279.4 225.425 0)
			(effects
				(font
					(size 1.27 1.27)
					(italic yes)
				)
				(justify left)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 273.05 223.52 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 273.05 223.52 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "cb7e94b3-e9b3-4555-a934-877901f1254d")
		)
		(pin "2"
			(uuid "6b55fbee-a95b-4175-8f69-377c43d5405d")
		)
		(pin "3"
			(uuid "37d31d22-2778-4e6b-8d14-9f8602707fb4")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "Q5")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:R")
		(at 219.71 223.52 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005afdb326")
		(property "Reference" "R29"
			(at 219.71 225.552 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "22"
			(at 219.71 223.52 90)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Resistors_perso:R_Axial_DIN0207_L40mm_D2.5mm_P15.24mm_Horizontal"
			(at 219.71 221.742 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 219.71 223.52 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 219.71 223.52 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "8d0ecf2c-24ff-4de8-add2-c81af7fd35f7")
		)
		(pin "2"
			(uuid "abc9f3a5-6544-4728-b915-064bb9d88528")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "R29")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 275.59 228.6 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005afdb6aa")
		(property "Reference" "#PWR024"
			(at 275.59 234.95 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 275.59 232.41 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 275.59 228.6 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 275.59 228.6 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 275.59 228.6 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "b4506f05-bb14-48b8-b4a9-61f0fa7f1c21")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR024")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:C")
		(at 260.35 73.66 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005b327d59")
		(property "Reference" "C8"
			(at 260.985 71.12 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "2.2u"
			(at 260.985 76.2 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm"
			(at 261.3152 77.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 260.35 73.66 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 260.35 73.66 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "cbc3a1d0-e642-43c0-8e43-690f84eb85dc")
		)
		(pin "2"
			(uuid "dc4888d6-032c-4e88-8013-90b8fdcdeab4")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "C8")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 260.35 77.47 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005b327e4b")
		(property "Reference" "#PWR025"
			(at 260.35 83.82 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 260.35 81.28 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 260.35 77.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 260.35 77.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 260.35 77.47 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "1d95a3a8-ce79-4cb1-b73d-36583cb4b301")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR025")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:C")
		(at 101.6 125.73 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005b32c700")
		(property "Reference" "C3"
			(at 102.235 123.19 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "100n"
			(at 102.235 128.27 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm"
			(at 102.5652 129.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 101.6 125.73 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 101.6 125.73 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "87f750c4-2728-43cd-88af-4051674b03f4")
		)
		(pin "1"
			(uuid "9acaccd3-9b39-4e7d-8dc4-16e77132f177")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "C3")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:GND-power")
		(at 101.6 129.54 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005b32c7e0")
		(property "Reference" "#PWR026"
			(at 101.6 135.89 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "GND"
			(at 101.6 133.35 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 101.6 129.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 101.6 129.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 101.6 129.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "4752cdbe-1fe3-4574-8ac1-8ccd51a574ee")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#PWR026")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:D")
		(at 190.5 179.07 180)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005b369317")
		(property "Reference" "D5"
			(at 190.5 181.61 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "1N5819"
			(at 190.5 176.53 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Diodes_THT:D_DO-15_P10.16mm_Horizontal"
			(at 190.5 179.07 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 190.5 179.07 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 190.5 179.07 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "2"
			(uuid "7fe18dd4-35c8-47db-b585-b01e5bf88775")
		)
		(pin "1"
			(uuid "2a40d025-75c3-4ad6-81da-a95faaa3bb55")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "D5")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Device:D")
		(at 190.5 190.5 180)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005b36955a")
		(property "Reference" "D4"
			(at 190.5 193.04 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "1N5819"
			(at 190.5 187.96 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Diodes_THT:D_DO-15_P10.16mm_Horizontal"
			(at 190.5 190.5 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" ""
			(at 190.5 190.5 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 190.5 190.5 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "f2470391-5fa6-4a6a-a9cf-432d6c057e99")
		)
		(pin "2"
			(uuid "3812178a-4589-47ca-9cc8-f2410b52911a")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "D4")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "RegulatorV2-rescue:PWR_FLAG-power")
		(at 149.86 139.7 0)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ef021c0")
		(property "Reference" "#FLG0101"
			(at 149.86 137.795 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Value" "PWR_FLAG"
			(at 149.86 135.2804 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" ""
			(at 149.86 139.7 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "~"
			(at 149.86 139.7 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 149.86 139.7 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "9a967525-3ad9-4741-9409-f8bd4380c7d6")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "#FLG0101")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Mechanical:MountingHole_Pad")
		(at 45.72 59.69 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ef185da")
		(property "Reference" "MH3"
			(at 49.53 58.5216 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "MountingHole_Pad"
			(at 49.53 60.833 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "MountingHole:MountingHole_5.3mm_M5_DIN965_Pad"
			(at 45.72 59.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "~"
			(at 45.72 59.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 45.72 59.69 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "04eceffe-a961-4517-b63f-590abbd32c3b")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "MH3")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Mechanical:MountingHole_Pad")
		(at 45.72 41.91 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ef18686")
		(property "Reference" "MH1"
			(at 49.53 40.7416 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "MountingHole_Pad"
			(at 49.53 43.053 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "MountingHole:MountingHole_5.3mm_M5_DIN965_Pad"
			(at 45.72 41.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "~"
			(at 45.72 41.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 45.72 41.91 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "16ffa822-2388-4444-b786-79403ec9a6fe")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "MH1")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Mechanical:MountingHole_Pad")
		(at 45.72 48.26 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ef18720")
		(property "Reference" "MH2"
			(at 49.53 47.0916 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "MountingHole_Pad"
			(at 49.53 49.403 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "MountingHole:MountingHole_5.3mm_M5_DIN965_Pad"
			(at 45.72 48.26 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "~"
			(at 45.72 48.26 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 45.72 48.26 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "eebc22ca-b660-4b97-bbcf-93a9cb2b36d4")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "MH2")
					(unit 1)
				)
			)
		)
	)
	(symbol
		(lib_id "Mechanical:MountingHole_Pad")
		(at 45.72 66.04 270)
		(unit 1)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(dnp no)
		(uuid "00000000-0000-0000-0000-00005ef187b4")
		(property "Reference" "MH4"
			(at 49.53 64.8716 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "MountingHole_Pad"
			(at 49.53 67.183 90)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "MountingHole:MountingHole_5.3mm_M5_DIN965_Pad"
			(at 45.72 66.04 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "~"
			(at 45.72 66.04 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" ""
			(at 45.72 66.04 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(pin "1"
			(uuid "fd3d23c6-d001-4012-883a-e108a97085a2")
		)
		(instances
			(project "RegulatorV2"
				(path "/b4cce7ed-771b-4bd3-a414-5049c1e45b26"
					(reference "MH4")
					(unit 1)
				)
			)
		)
	)
	(sheet_instances
		(path "/"
			(page "1")
		)
	)
)
